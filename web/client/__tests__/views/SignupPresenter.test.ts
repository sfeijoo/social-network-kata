import { SignupPresenter, SignupView } from '../../src/views/signup/SignupPresenter';
import { SignupVM } from '../../src/views/signup/SignupVM';
import { anything, instance, verify, when } from 'ts-mockito';
import { Signup } from '../../src/actions/Signup';
import { mockEq } from '../lib/ts-mockito-extensions';
import { ValidationError } from '../../src/model/ValidationError';

test('changes username', () => {
    presenter.changeUsername('bob');

    expect(view.model.username).toEqual('bob');
});

test('changes password', () => {
    presenter.changePassword('4321');

    expect(view.model.password).toEqual('4321');
});

test('signup should execute signup action with entered credentials', async () => {
    presenter.changeUsername('bob');
    presenter.changePassword('1234');

    await presenter.signup();

    verify(signup.exec('bob', '1234')).called();
});

test('show loading while doing signup', async () => {
    await presenter.signup();

    expect(view.hasLoaded).toEqual(true);
    expect(view.model.isLoading).toEqual(false);
});

test('show general error when login fails', async () => {
    when(signup.exec(anything(), anything())).thenReject(new ValidationError('', 'Some general error'));

    await presenter.signup();

    expect(view.model.errors['']).toEqual('Some general error');
});

beforeEach(() => {
    view = new ViewStub();
    signup = mockEq<Signup>();
    presenter = new SignupPresenter(view, instance(signup));
});

let view: ViewStub;
let presenter: SignupPresenter;
let signup: Signup;

class ViewStub implements SignupView {
    model = new SignupVM();
    hasLoaded = false;

    onModelChanged(model: SignupVM) {
        this.model = model;
        if (model.isLoading) this.hasLoaded = true;
    }
}
