import { instance, verify, when } from 'ts-mockito';
import { TimelinePresenter,  TimelineView } from '../../src/views/timeline/TimelinePresenter';
import { TimelineVM } from '../../src/views/timeline/TimelineVM';
import { Timeline } from '../../src/actions/Timeline';
import { mockEq } from '../lib/ts-mockito-extensions';
import { ValidationError } from '../../src/model/ValidationError';
import { alice } from '../model/UserExamples';

test('timeline should execute timeline action for logged user', async () => {
    await presenter.getTimeline();

    verify(timeline.exec()).called();
});

test('show general error when timeline fails', async () => {
    when(timeline.exec()).thenReject(new ValidationError('', 'Some general error'));

    await presenter.getTimeline();

    expect(view.model.errors['']).toEqual('Some general error');
});

test('show loading while retrieving timeline', async () => {
    await presenter.getTimeline();

    expect(view.hasLoaded).toEqual(true);
    expect(view.model.isLoading).toEqual(false);
});

test('show timeline', async () => {
    when(timeline.exec()).thenResolve(createResponse(alice.username, message, date));

    await presenter.getTimeline();

    const post = view.model.timeline.pop()!;
    expect(post.username).toEqual(alice.username);
    expect(post.message).toEqual(message);
    expect(post.date).toEqual(date);
});

function createResponse(username: string, message: string, date: string): string {
    const json = {
        "posts":[
            {
                "username": username,
                "message": message,
                "date": date,
            }
        ]
    };
    return JSON.stringify(json);
}

beforeEach(() => {
    view = new ViewStub();
    timeline = mockEq<Timeline>();
    when(timeline.exec()).thenResolve(createResponse(alice.username, message, date));
    presenter = new TimelinePresenter(view, instance(timeline));
});

let view: ViewStub;
let presenter: TimelinePresenter;
let timeline: Timeline;
const message = "a message";
const date = new Date().toDateString();

class ViewStub implements TimelineView {
    model = new TimelineVM();
    hasLoaded = false;

    onModelChanged(model: TimelineVM) {
        this.model = model;
        if (model.isLoading) this.hasLoaded = true;
    }
}