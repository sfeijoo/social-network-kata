import { LoginPresenter, LoginView } from '../../src/views/login/LoginPresenter';
import { LoginVM } from '../../src/views/login/LoginVM';
import { anything, instance, verify, when } from 'ts-mockito';
import { mockEq } from '../lib/ts-mockito-extensions';
import { Login } from '../../src/actions/Login';
import { ValidationError } from '../../src/model/ValidationError';
import each from 'jest-each';

test('changes username', () => {
    presenter.changeUsername('bob');

    expect(view.model.username).toEqual('bob');
});

test('changes password', () => {
    presenter.changePassword('4321');

    expect(view.model.password).toEqual('4321');
});

test('login should execute login action with entered credentials', async () => {
    presenter.changeUsername('alice');
    presenter.changePassword('1234');

    await presenter.login();

    verify(login.exec('alice', '1234')).called();
});

test('show loading while doing login', async () => {
    await presenter.login();

    expect(view.hasLoaded).toEqual(true);
    expect(view.model.isLoading).toEqual(false);
});

each([
    'username', 'password'
]).test('show %s error when login fails', async (property) => {
    when(login.exec(anything(), anything())).thenReject(new ValidationError(property, 'Some error'));

    await presenter.login();

    expect(view.model.errors[property]).toEqual('Some error');
});

test('show general error when login fails', async () => {
    when(login.exec(anything(), anything())).thenReject(new ValidationError('', 'Some general error'));

    await presenter.login();

    expect(view.model.errors['']).toEqual('Some general error');
});

beforeEach(() => {
    view = new ViewStub();
    login = mockEq<Login>();
    presenter = new LoginPresenter(view, instance(login));
});

let view: ViewStub;
let presenter: LoginPresenter;
let login: Login;

class ViewStub implements LoginView {
    model = new LoginVM();
    hasLoaded = false;

    onModelChanged(model: LoginVM) {
        this.model = model;
        if (model.isLoading) this.hasLoaded = true;
    }
}
