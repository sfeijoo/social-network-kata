import { HttpAuthService } from '../../src/infrastructure/http/HttpAuthService';
import { alice } from '../model/UserExamples';
import { anything, instance, verify, when } from 'ts-mockito';
import { HttpClient } from '../../src/infrastructure/http/HttpClient';
import { mockEq } from '../lib/ts-mockito-extensions';
import { HttpResponse } from '../../src/infrastructure/http/HttpResponse';
import { expectThrows } from '../lib/expectThrows';
import { UserAlreadyExistsError } from '../../src/model/UserAlreadyExistsError';
import { HttpError } from '../../src/infrastructure/http/HttpError';

describe('login', () => {
    test('should make a post to /login with credentials', async () => {
        await authService.login(alice.username, alice.password);

        verify(httpClient.post('/login', { username: alice.username, password: alice.password })).called();
    });

    test('should return session token on success', async () => {
        const httpResponse = new HttpResponse(200, { isSuccessful: true, sessionToken: alice.sessionToken });
        when(httpClient.post('/login', anything())).thenResolve(httpResponse);

        const response = await authService.login(alice.username, alice.password);

        expect(response.isSuccessful).toEqual(true);
        expect(response.sessionToken).toEqual(alice.sessionToken);
    });

    test('should return unsuccessful response when fails', async () => {
        const httpResponse = new HttpResponse(200, { isSuccessful: false, sessionToken: null });
        when(httpClient.post('/login', anything())).thenResolve(httpResponse);

        const response = await authService.login(alice.username, alice.password);

        expect(response.isSuccessful).toEqual(false);
        expect(response.sessionToken).toBeNull();
    });
});

describe('signup', () => {
    test('should make a post to /login with credentials', async () => {
        await authService.signup(alice.username, alice.password);

        verify(httpClient.post('/users', { username: alice.username, password: alice.password })).called();
    });

    test('should fail when signup fails', async () => {
        const errorBody = { type: 'UserAlreadyExistsError', message: 'User already exists' };
        when(httpClient.post('/users', anything())).thenReject(new HttpError(400, errorBody));

        await expectThrows(async () => {
            await authService.signup(alice.username, alice.password);
        }, UserAlreadyExistsError);
    });
});

beforeEach(() => {
    httpClient = mockEq<HttpClient>();
    when(httpClient.post('/login', anything())).thenResolve(loginSuccessResponse);
    authService = new HttpAuthService(instance(httpClient));
    const httpResponse = new HttpResponse(200, { userId: 1 });
    when(httpClient.post('/users', anything())).thenResolve(httpResponse);
});

let httpClient: HttpClient;
let authService: HttpAuthService;
const loginSuccessResponse = new HttpResponse(200, { isSuccessful: true, sessionToken: alice.sessionToken });
