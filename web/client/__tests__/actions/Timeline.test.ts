import { when } from 'ts-mockito';
import { expectThrows } from '../lib/expectThrows';
import { ValidationError } from '../../src/model/ValidationError';
import { Timeline } from '../../src/actions/Timeline';
import { SessionStorage } from '../../src/model/SessionStorage';
import { TimelineService } from '../../src/model/TimelineService';

test('fail if not authenticated', async () => {
    when(timelineService.getTimeline()).thenResolve();

    await expectThrows(async () => {
        await timeline.exec();
    }, ValidationError);
});

let timeline: Timeline;
let timelineService: TimelineService;
let sessionStorage: SessionStorage;
