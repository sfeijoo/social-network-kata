import { anything, instance, verify, when } from 'ts-mockito';
import { alice } from '../model/UserExamples';
import { AuthService } from '../../src/model/AuthService';
import { Signup } from '../../src/actions/Signup';
import { mockEq } from '../lib/ts-mockito-extensions';
import { expectThrows } from '../lib/expectThrows';
import { ValidationError } from '../../src/model/ValidationError';
import { UserAlreadyExistsError } from '../../src/model/UserAlreadyExistsError';

test('register user with given credentials', async () => {
    when(authService.signup(alice.username, alice.password)).thenResolve({ userId: 1 });

    await signup.exec(alice.username, alice.password);

    verify(authService.signup(alice.username, alice.password)).called();
});

test('username is required', async () => {
    const username = '';

    await expectThrows(async () => {
        await signup.exec(username, alice.password);
    }, ValidationError);
});

test('password is required', async () => {
    const password = '';

    await expectThrows(async () => {
        await signup.exec(alice.username, password);
    }, ValidationError);
});

test('return error with already existing user', async () => {
    when(authService.signup(anything(), anything())).thenReject(new UserAlreadyExistsError('User already exists'));

    await expectThrows(async () => {
        await signup.exec(alice.username, alice.password);
    }, ValidationError);
});

beforeEach(() => {
    authService = mockEq<AuthService>();
    signup = new Signup(instance(authService));
});

let signup: Signup;
let authService: AuthService;
