import { Login } from '../../src/actions/Login';
import { alice } from '../model/UserExamples';
import { anything, instance, when } from 'ts-mockito';
import { AuthService } from '../../src/model/AuthService';
import { SessionStorage } from '../../src/model/SessionStorage';
import { mockEq } from '../lib/ts-mockito-extensions';
import { InMemorySessionStorage } from '../../src/infrastructure/InMemorySessionStorage';
import { expectThrows } from '../lib/expectThrows';
import { ValidationError } from '../../src/model/ValidationError';

test('authenticate user with given credentials', async () => {
    when(authService.login(alice.username, alice.password)).thenResolve({ isSuccessful: true, sessionToken: alice.sessionToken });

    await login.exec(alice.username, alice.password);

    const currentSession = sessionStorage.get();
    expect(currentSession.isAuthenticated).toEqual(true);
    expect(currentSession.sessionToken).toEqual(alice.sessionToken);
});

test('username is required', async () => {
    const username = '';

    await expectThrows(async () => {
        await login.exec(username, alice.password);
    }, ValidationError);
});

test('password is required', async () => {
    const password = '';

    await expectThrows(async () => {
        await login.exec(alice.username, password);
    }, ValidationError);
});

test('return error with invalid credentials', async () => {
    when(authService.login(anything(), anything())).thenResolve({ isSuccessful: false, sessionToken: null });

    await expectThrows(async () => {
        await login.exec(alice.username, alice.password);
    }, ValidationError);
});

beforeEach(() => {
    authService = mockEq<AuthService>();
    sessionStorage = new InMemorySessionStorage();
    login = new Login(instance(authService), sessionStorage);
});

let login: Login;
let authService: AuthService;
let sessionStorage: SessionStorage;
