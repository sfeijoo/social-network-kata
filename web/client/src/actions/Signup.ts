import { AuthService } from '../model/AuthService';
import { ValidationError } from '../model/ValidationError';
import { UserAlreadyExistsError } from '../model/UserAlreadyExistsError';

export class Signup {
    constructor(private authService: AuthService) {}

    async exec(username: string, password: string): Promise<void> {
        this.validate(username, password);
        try {
            await this.authService.signup(username, password);
        } catch (e) {
            if (e instanceof UserAlreadyExistsError) throw new ValidationError('', 'User already exists');
            throw e;
        }
    }

    private validate(username: string, password: string) {
        if (this.isBlankOrEmpty(username)) throw new ValidationError('username', 'Username is required');
        if (this.isBlankOrEmpty(password)) throw new ValidationError('password', 'Password is required');
    }

    private isBlankOrEmpty(value: string) {
        return value.trim().length == 0;
    }
}