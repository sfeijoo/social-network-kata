import { AuthService } from '../model/AuthService';
import { SessionStorage } from '../model/SessionStorage';
import { ValidationError } from '../model/ValidationError';

export class Login {
    constructor(private authService: AuthService, private sessionStorage: SessionStorage) {}

    async exec(username: string, password: string): Promise<void> {
        this.validate(username, password);
        const sessionToken = await this.doLogin(username, password);
        this.authenticate(sessionToken);
    }

    private async doLogin(username: string, password: string) {
        const response = await this.authService.login(username, password);
        if (!response.isSuccessful) throw new ValidationError('', 'Invalid credentials');
        return response.sessionToken!;
    }

    private authenticate(sessionToken: string) {
        const session = this.sessionStorage.get();
        session.authenticate(sessionToken);
        this.sessionStorage.save(session);
    }

    private validate(username: string, password: string) {
        if (this.isBlankOrEmpty(username)) throw new ValidationError('username', 'Username is required');
        if (this.isBlankOrEmpty(password)) throw new ValidationError('password', 'Password is required');
    }

    private isBlankOrEmpty(value: string) {
        return value.trim().length == 0;
    }
}
