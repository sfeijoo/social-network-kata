export interface AuthService {
    login(username: string, password: string): Promise<LoginResponse>;
    signup(username: string, password: string): Promise<SignupResponse>;
}

export interface LoginResponse {
    isSuccessful: boolean;
    sessionToken: string|null;
}

export interface SignupResponse {
    userId: number;
}
