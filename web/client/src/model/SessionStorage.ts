import { Session } from './Session';

export interface SessionStorage {
    get(): Session;
    save(currentSession: Session);
}
