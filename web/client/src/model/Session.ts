export class Session {
    private _isAuthenticated = false;
    private _sessionToken: string|null = null;

    get sessionToken(): string | null {
        return this._sessionToken;
    }

    get isAuthenticated(): boolean {
        return this._isAuthenticated;
    }

    authenticate(sessionToken: string) {
        this._sessionToken = sessionToken;
        this._isAuthenticated = true;
    }
}
