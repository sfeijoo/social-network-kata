import { CustomError } from 'ts-custom-error';

export class UserAlreadyExistsError extends CustomError {
    constructor(message: string) {
        super(message);
    }
}
