import { HttpClient } from './HttpClient';
import axios from 'axios';
import { HttpResponse } from './HttpResponse';
import { HttpError } from './HttpError';

export class AxiosHttpClient implements HttpClient {
    constructor(private baseUrl: string) {}

    async post(url: string, body: any): Promise<HttpResponse> {
        try {
            const response = await axios.post(this.baseUrl + url, body);
            return new HttpResponse(response.status, response.data);
        } catch (e) {
            throw new HttpError(e.response?.status ?? 0, e.response?.data);
        }
    }
}
