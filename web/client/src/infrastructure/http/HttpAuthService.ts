import { AuthService, LoginResponse, SignupResponse } from '../../model/AuthService';
import { HttpClient } from './HttpClient';
import { HttpError } from './HttpError';
import { UserAlreadyExistsError } from '../../model/UserAlreadyExistsError';

export class HttpAuthService implements AuthService {
    constructor(private httpClient: HttpClient) {}

    async login(username: string, password: string): Promise<LoginResponse> {
        const response = await this.httpClient.post('/login', { username, password });
        return response.body;
    }

    async signup(username: string, password: string): Promise<SignupResponse> {
        try {
            const response = await this.httpClient.post('/users', { username, password });
            return response.body;
        } catch (e) {
            if (e instanceof HttpError && e.statusCode == 400 && e.body.type == 'UserAlreadyExistsError') {
                throw new UserAlreadyExistsError(e.body.message);
            }
            throw e;
        }
    }
}
