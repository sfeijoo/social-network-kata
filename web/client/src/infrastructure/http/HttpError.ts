import { CustomError } from 'ts-custom-error';

export class HttpError extends CustomError {
    constructor(public statusCode: number, public body: any) {
        super(`Http error: ${statusCode}`);
    }
}