import { SessionStorage } from '../model/SessionStorage';
import { Session } from '../model/Session';

export class InMemorySessionStorage implements SessionStorage {
    private currentSession = new Session();

    get(): Session {
        return this.currentSession;
    }

    save(currentSession: Session) {
        this.currentSession = currentSession;
    }
}
