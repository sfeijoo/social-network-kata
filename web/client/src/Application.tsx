import './views/app/base.css';
import ReactDOM from 'react-dom';
import { AppRoot } from './views/app/AppRoot';
import React from 'react';
import { AxiosHttpClient } from './infrastructure/http/AxiosHttpClient';
import { Config } from './infrastructure/Config';
import { PresenterFactory } from './views/app/PresenterFactory';
import { Login } from './actions/Login';
import { HttpAuthService } from './infrastructure/http/HttpAuthService';
import { InMemorySessionStorage } from './infrastructure/InMemorySessionStorage';
import AppContext from './views/app/AppContext';
import { Signup } from './actions/Signup';

export class Application {
    private httpClient = new AxiosHttpClient(Config.get('apiBaseUrl'));
    private authService = new HttpAuthService(this.httpClient);
    private sessionStorage = new InMemorySessionStorage();
    private loginAction = new Login(this.authService, this.sessionStorage);
    private signupAction = new Signup(this.authService);
    private presenterFactory = new PresenterFactory(this.loginAction, this.signupAction);

    constructor() {
        AppContext.presenters = this.presenterFactory;
        // @ts-ignore
        window.app = this;
    }

    render() {
        ReactDOM.render(<AppRoot />, document.getElementById('root'));
    }
}
