import { LoginPresenter, LoginView } from '../login/LoginPresenter';
import { Login } from '../../actions/Login';
import { Signup } from '../../actions/Signup';
import { SignupPresenter, SignupView } from '../signup/SignupPresenter';

export class PresenterFactory {
    constructor(private loginAction: Login, private signupAction: Signup) {
    }

    login(view: LoginView) {
        return new LoginPresenter(view, this.loginAction);
    }

    signup(view: SignupView) {
        return new SignupPresenter(view, this.signupAction);
    }
}
