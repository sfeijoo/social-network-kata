import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const MainMenu = () => (
    <Nav>
        <Link to="/">Home</Link>
        <Link to="/login">Login</Link>
        <Link to="/signup">Signup</Link>
    </Nav>
);

const Nav = styled.nav`
    a {
      display: inline-block;
      margin-right: 15px;
      color: #000;
      text-decoration: none;
      
      &:hover {
        color: #ccc;
      }
    }
`;
