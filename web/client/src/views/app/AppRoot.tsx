import React from 'react';
import { LoginPage } from '../login/LoginPage';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { HomePage } from '../home/HomePage';
import { MainMenu } from './MainMenu';
import { SignupPage } from '../signup/SignupPage';

export const AppRoot = () => (
    <BrowserRouter>
        <div>
            <header>
                <h1>Social Network</h1>
                <MainMenu />
            </header>
            <Switch>
                <Route path="/login" component={LoginPage} />
                <Route path="/signup" component={SignupPage} />
                <Route path="/" component={HomePage} />
            </Switch>
        </div>
    </BrowserRouter>
);
