import React from 'react';
import { TextField } from '../components/TextField';
import { SignupView } from './SignupPresenter';
import { SignupVM } from './SignupVM';
import AppContext from '../app/AppContext';

export class SignupPage extends React.Component<any, State> implements SignupView {
    presenter = AppContext.presenters.signup(this);
    state = {
        model: new SignupVM(),
    };

    onModelChanged(model: SignupVM) {
        this.setState( { model } );
    }

    render() {
        if (this.state.model.isLoading) return <div>Loading</div>;
        return (
            <div>
                <h2>Sign up</h2>

                {this.state.model.errors[''] && <div>{this.state.model.errors['']}</div>}

                <TextField
                    label="Usuario:"
                    value={this.state.model.username}
                    onChange={value => this.presenter.changeUsername(value)}
                    error={this.state.model.errors['usuario']}
                />

                <TextField
                    label="Contraseña:"
                    value={this.state.model.password}
                    onChange={value => this.presenter.changePassword(value)}
                    error={this.state.model.errors['password']}
                />

                <button onClick={() => this.presenter.signup()}>Signup</button>
            </div>
        );
    }
}

interface State {
    model: SignupVM;
}