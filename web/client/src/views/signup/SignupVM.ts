export class SignupVM {
    username = '';
    password = '';
    isLoading = false;
    errors: { [name: string]: string } = {};
}