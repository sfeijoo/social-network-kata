import { SignupVM } from './SignupVM';
import { Signup } from '../../actions/Signup';
import { ValidationError } from '../../model/ValidationError';

export interface SignupView {
    onModelChanged(model: SignupVM);
}

export class SignupPresenter {
    private model = new SignupVM();

    constructor(private view: SignupView, private signupAction: Signup) {}

    async signup() {
        this.model.isLoading = true;
        this.notifyChange();
        try {
            await this.signupAction.exec(this.model.username, this.model.password);
        } catch (e) {
            if (!(e instanceof ValidationError)) throw e;
            this.model.errors[e.property] = e.message;
        }
        this.model.isLoading = false;
        this.notifyChange();
    }

    changeUsername(username: string) {
        this.model.username = username;
        this.notifyChange();
    }

    changePassword(password: string) {
        this.model.password = password;
        this.notifyChange();
    }

    private notifyChange() {
        this.view.onModelChanged(this.model);
    }
}