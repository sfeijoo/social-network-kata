import React from 'react';
import { LoginView } from './LoginPresenter';
import { LoginVM } from './LoginVM';
import AppContext from '../app/AppContext';
import { TextField } from '../components/TextField';

export class LoginPage extends React.Component<any, State> implements LoginView {
    presenter = AppContext.presenters.login(this);
    state = {
        model: new LoginVM(),
    };

    onModelChanged(model: LoginVM) {
        this.setState({ model });
    }

    render() {
        if (this.state.model.isLoading) return <div>Loading</div>;
        return (
            <div>
                <h2>Login</h2>

                {this.state.model.errors[''] && <div>{this.state.model.errors['']}</div>}

                <TextField
                    label="Usuario:"
                    value={this.state.model.username}
                    onChange={value => this.presenter.changeUsername(value)}
                    error={this.state.model.errors['usuario']}
                />
                <TextField
                    label="Contraseña:"
                    value={this.state.model.password}
                    onChange={value => this.presenter.changePassword(value)}
                    error={this.state.model.errors['password']}
                />

                <button onClick={() => this.presenter.login()}>Login</button>
            </div>
        );
    }
}

interface State {
    model: LoginVM;
}
