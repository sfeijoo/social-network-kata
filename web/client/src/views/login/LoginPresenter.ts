import { LoginVM } from './LoginVM';
import { Login } from '../../actions/Login';
import { ValidationError } from '../../model/ValidationError';

export interface LoginView {
    onModelChanged(model: LoginVM);
}

export class LoginPresenter {
    private model = new LoginVM();

    constructor(private view: LoginView, private loginAction: Login) {}

    async login() {
        this.model.isLoading = true;
        this.notifyChange();
        try {
            await this.loginAction.exec(this.model.username, this.model.password);
        } catch (e) {
            if (!(e instanceof ValidationError)) throw e;
            this.model.errors[e.property] = e.message;
        }
        this.model.isLoading = false;
        this.notifyChange();
    }

    changeUsername(username: string) {
        this.model.username = username;
        this.notifyChange();
    }

    changePassword(password: string) {
        this.model.password = password;
        this.notifyChange();
    }

    private notifyChange() {
        this.view.onModelChanged(this.model);
    }
}
