export class LoginVM {
    username = '';
    password = '';
    isLoading = false;
    errors: { [name: string]: string } = {};
}
