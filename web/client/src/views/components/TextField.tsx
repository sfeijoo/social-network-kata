import React from 'react';
import styled from 'styled-components';

export const TextField: React.FC<Props> = (props) => (
    <Container>
        <label>{props.label}</label>
        <input type="text" value={props.value} onChange={e => props.onChange(e.target.value)} />
        {props.error && <div>{props.error}</div>}
    </Container>
);

const Container = styled.div`
  display: flex;
  flex-direction: column;
  
  label {
    font-weight: bold;
  }
  
  input {
    max-width: 200px;
  }
`;

interface Props {
    label: string;
    value: string;
    onChange: (value: string) => void;
    error?: string|null;
}
