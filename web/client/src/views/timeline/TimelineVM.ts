export class PostVM {
    constructor(public username: string, public message: string, public date: string) {}
}

export class TimelineVM {
    isLoading = false;
    errors: { [name: string]: string } = {};
    timeline: Array<PostVM> = [];
}