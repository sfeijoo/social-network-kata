import { PostVM, TimelineVM } from './TimelineVM';
import { Timeline } from '../../actions/Timeline';
import { ValidationError } from '../../model/ValidationError';

export interface TimelineView {
    onModelChanged(model: TimelineVM);
}

export class TimelinePresenter {
    private model = new TimelineVM();

    constructor(private view: TimelineView, private timelineAction: Timeline) {}

    async getTimeline() {
        this.model.isLoading = true;
        this.notifyChange();
        try {
            const response = await this.timelineAction.exec();
            this.updateTimeline(response);
        } catch (e) {
            if (!(e instanceof ValidationError)) throw e;
            this.model.errors[e.property] = e.message;
        }
        this.model.isLoading = false;
        this.notifyChange();
    }
    
    private updateTimeline(response: string) {
        const json = JSON.parse(response);
        json.posts.forEach(it => {
            this.model.timeline.push(new PostVM(it.username, it.message, it.date));
        });
    }

    private notifyChange() {
        this.view.onModelChanged(this.model);
    }
}