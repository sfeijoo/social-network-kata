package core.domain

abstract class DomainError(message: String? = null): Exception(message)
