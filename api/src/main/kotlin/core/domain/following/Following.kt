package core.domain.following

import core.domain.user.UserId

class Following(val id: FollowingId, val userId: UserId, val followsId: UserId) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Following

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "Following(id=$id, userId=$userId, followsId=$followsId)"
    }
}
