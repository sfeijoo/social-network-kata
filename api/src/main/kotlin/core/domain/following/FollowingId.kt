package core.domain.following

class FollowingId(private val value: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FollowingId

        if (value != other.value) return false

        return true
    }

    override fun hashCode() = value

    override fun toString() = "FollowingId($value)"

    fun toInt() = value
}
