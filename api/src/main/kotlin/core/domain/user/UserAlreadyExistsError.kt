package core.domain.user

import core.domain.DomainError

class UserAlreadyExistsError(userName: String) : DomainError("User $userName already exists")

