package core.domain.user

class UserId(private val value: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserId

        if (value != other.value) return false

        return true
    }

    override fun hashCode() = value

    override fun toString() = "UserId($value)"

    fun toInt() = value
}
