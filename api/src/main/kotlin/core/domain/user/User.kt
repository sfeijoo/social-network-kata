package core.domain.user

import java.time.LocalDateTime

class User(val id: UserId, val userName: String, val password: String) {
    var createdAt: LocalDateTime = LocalDateTime.now()
        private set
    var sessionToken = ""

    fun snapshot() = UserSnapshot(id.toInt(), userName, password, sessionToken, createdAt)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    data class UserSnapshot(
        val id: Int,
        val userName: String,
        val password: String,
        val sessionToken: String,
        val createdAt: LocalDateTime
    )

    companion object {
        fun from(snapshot: UserSnapshot): User {
            val user = User(UserId(snapshot.id), snapshot.userName, snapshot.password)
            user.createdAt = snapshot.createdAt
            user.sessionToken = snapshot.sessionToken
            return user
        }
    }
}
