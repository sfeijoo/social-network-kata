package core.domain

import kotlin.reflect.KClass

interface RepositoryFactory {
    fun <T: Any> get(repositoryType: KClass<T>): T
}