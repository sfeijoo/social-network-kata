package core.domain

import core.domain.post.Post
import core.domain.post.PostId
import core.domain.user.UserId

interface PostRepository {
    fun add(post: Post)
    fun nextId(): PostId
    fun get(postId: PostId): Post
    fun getAllPosts(): List<Post>
    fun getByUserId(userId: UserId): List<Post>
}