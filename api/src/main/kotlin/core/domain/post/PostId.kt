package core.domain.post

class PostId(private val value: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PostId

        if (value != other.value) return false

        return true
    }

    override fun hashCode() = value

    override fun toString() = "PostId($value)"

    fun toInt() = value
}
