package core.domain.post

import core.domain.user.User
import core.domain.user.UserId
import java.time.LocalDateTime

class Post(val id: PostId, val userId: UserId, val message: String, val date: LocalDateTime) {

    fun snapshot() = Post.PostSnapshot(id.toInt(), userId.toInt(), message, date)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Post

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    data class PostSnapshot(
        val id: Int,
        val userId: Int,
        val message: String,
        val createdAt: LocalDateTime,
    )

    companion object {
        fun from(snapshot: PostSnapshot): Post {
            return Post(PostId(snapshot.id), UserId(snapshot.userId), snapshot.message, snapshot.createdAt)
        }
    }

}
