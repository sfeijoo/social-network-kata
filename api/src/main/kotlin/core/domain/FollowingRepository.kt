package core.domain

import core.domain.following.Following
import core.domain.following.FollowingId
import core.domain.user.UserId

interface FollowingRepository {
    fun add(following: Following)
    fun findByUser(userId: UserId): List<Following>
    fun findByUsers(userId: UserId, followsId: UserId): Following?
    fun remove(following: Following)
    fun nextId(): FollowingId
    fun get(id: FollowingId): Following
}