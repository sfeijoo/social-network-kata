package core.domain

import core.domain.user.User
import core.domain.user.UserId

interface UserRepository {
    fun get(userName: String): User
    fun getBySessionToken(token: String): User
    fun getById(userId: UserId): User
    fun find(username: String): Boolean
    fun findByToken(sessionToken: String): Boolean
    fun add(user: User)
    fun nextId(): UserId
}
