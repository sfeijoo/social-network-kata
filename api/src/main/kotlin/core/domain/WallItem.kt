package core.domain

import core.domain.post.Post
import core.domain.post.PostId
import core.domain.user.UserId
import java.time.LocalDateTime

class WallItem (val id: PostId, val userId: UserId, val userName: String, val message: String, val date: LocalDateTime) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Post

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
