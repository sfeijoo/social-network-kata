package core.infrastructure

import core.domain.RepositoryFactory
import core.infrastructure.auth.UUIDSessionTokenGenerator
import core.infrastructure.inMemory.InMemoryRepositoryFactory
import core.useCases.Clock
import core.useCases.SessionTokenGenerator

data class CoreConfiguration(
    val clock: Clock,
    val sessionTokenGenerator: SessionTokenGenerator = UUIDSessionTokenGenerator(),
    val repositoryFactory: RepositoryFactory = InMemoryRepositoryFactory()
)
