package core.infrastructure.serialization

import com.google.gson.*
import core.infrastructure.time.LocalDateTimeParser
import core.infrastructure.time.formatAsISO8601
import java.lang.reflect.Type
import java.time.LocalDateTime

class LocalDateTimeSerializer: JsonSerializer<LocalDateTime?>, JsonDeserializer<LocalDateTime?> {
    override fun serialize(
        localDateTime: LocalDateTime?,
        srcType: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        return JsonPrimitive(localDateTime?.formatAsISO8601())
    }

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): LocalDateTime {
        return LocalDateTimeParser().parseISO8601(json.asString)
    }
}