package core.infrastructure.inMemory

import core.domain.following.Following
import core.domain.FollowingRepository
import core.domain.following.FollowingId
import core.domain.user.UserId
import core.useCases.follow.UnableToFollowError

class InMemoryFollowingRepository: FollowingRepository {
    private val followings = mutableListOf<Following>()
    private var nextId = 1

    override fun add(following: Following) {
        if (findByUsers(following.userId, following.followsId) != null) throw UnableToFollowError("Already following that user")
        followings.add(following)
    }

    override fun get(id: FollowingId): Following {
        return followings.single { it.id == id }
    }

    override fun findByUser(userId: UserId) = followings.filter { it.userId == userId }

    override fun findByUsers(userId: UserId, followsId: UserId): Following? {
        val following = findByUser(userId).filter { it.followsId == followsId }
        if (following.isNotEmpty()) return following[0]
        return null
    }

    override fun remove(following: Following) {
        followings.remove(following)
    }

    override fun nextId() = FollowingId(nextId++)

}