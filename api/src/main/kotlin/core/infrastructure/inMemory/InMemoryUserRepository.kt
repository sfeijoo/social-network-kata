package core.infrastructure.inMemory

import core.domain.*
import core.domain.user.User
import core.domain.user.UserAlreadyExistsError
import core.domain.user.UserId
import core.domain.user.UserNotFoundError

class InMemoryUserRepository: UserRepository {
    private val users = mutableListOf<User>()
    private var nextId = 1

    override fun get(userName: String) =
        users.singleOrNull { it.userName == userName } ?: throw UserNotFoundError()

    override fun getBySessionToken(sessionToken: String) =
        users.singleOrNull { it.sessionToken == sessionToken } ?: throw UserNotFoundError()

    override fun getById(userId: UserId) =
        users.singleOrNull { it.id == userId } ?: throw UserNotFoundError()

    override fun find(username: String): Boolean {
        users.find { it.userName == username } ?: return false
        return true
    }

    override fun findByToken(sessionToken: String): Boolean {
        users.find { it.sessionToken == sessionToken } ?: return false
        return true
    }

    override fun add(user: User) {
        if (users.any { it.userName == user.userName }) throw UserAlreadyExistsError(user.userName)
        users.add(user)
    }

    override fun nextId() = UserId(nextId++)

}