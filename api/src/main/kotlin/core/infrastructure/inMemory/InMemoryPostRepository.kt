package core.infrastructure.inMemory

import core.domain.*
import core.domain.post.Post
import core.domain.post.PostId
import core.domain.post.PostNotFoundError
import core.domain.user.UserId

class InMemoryPostRepository: PostRepository {
    var posts = mutableListOf<Post>()
    private var nextId = 1

    override fun add(post: Post) {
        posts.add(post)
    }

    override fun nextId(): PostId {
        return PostId(nextId++)
    }

    override fun get(postId: PostId): Post {
       return posts.singleOrNull { it.id == postId } ?: throw PostNotFoundError()
    }

    override fun getAllPosts() = posts

    override fun getByUserId(userId: UserId) = posts.filter { it.userId == userId }
}
