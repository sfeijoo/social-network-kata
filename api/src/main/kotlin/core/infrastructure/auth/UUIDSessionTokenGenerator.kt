package core.infrastructure.auth

import core.useCases.SessionTokenGenerator
import java.util.*

class UUIDSessionTokenGenerator: SessionTokenGenerator  {
    override fun create() = UUID.randomUUID().toString()
}
