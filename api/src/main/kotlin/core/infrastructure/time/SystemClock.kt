package core.infrastructure.time

import core.useCases.Clock
import java.time.LocalDateTime

class SystemClock: Clock {
    override fun now(): LocalDateTime = LocalDateTime.now()
}