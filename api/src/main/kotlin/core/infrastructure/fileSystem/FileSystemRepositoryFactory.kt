package core.infrastructure.fileSystem

import core.domain.FollowingRepository
import core.domain.PostRepository
import core.domain.RepositoryFactory
import core.domain.UserRepository
import core.infrastructure.inMemory.InMemoryFollowingRepository
import core.infrastructure.inMemory.InMemoryPostRepository
import kotlin.reflect.KClass

class FileSystemRepositoryFactory: RepositoryFactory {
    private val cache = mutableMapOf<KClass<*>, Any>()

    override fun <T: Any> get(repositoryType: KClass<T>): T {
        return cache.getOrPut(repositoryType) { create(repositoryType) } as T
    }

    private fun <T: Any> create(repositoryType: KClass<T>): T {
        return when(repositoryType) {
            UserRepository::class -> FileSystemUserRepository()
            PostRepository::class -> InMemoryPostRepository()
            FollowingRepository::class -> InMemoryFollowingRepository()
            else -> throw NotImplementedError()
        } as T
    }

}