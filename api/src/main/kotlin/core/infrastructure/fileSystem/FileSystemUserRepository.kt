package core.infrastructure.fileSystem

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import core.domain.UserRepository
import core.domain.user.User
import core.domain.user.UserId
import core.infrastructure.serialization.LocalDateTimeSerializer
import java.io.File
import java.nio.file.Paths
import java.time.LocalDateTime

class FileSystemUserRepository: UserRepository {
    private val storagePath = Paths.get("storage/users").toAbsolutePath().toString()

    override fun get(userName: String): User {
        return allUsers().single { it.userName == userName }
    }

    override fun getBySessionToken(token: String): User {
        return allUsers().single { it.sessionToken == token }
    }

    override fun getById(userId: UserId): User {
        val file = File("$storagePath/${userId.toInt()}.json")
        val json = file.readText(Charsets.UTF_8)
        val snapshot = gson().fromJson(json, User.UserSnapshot::class.java)
        return User.from(snapshot)
    }

    override fun find(username: String): Boolean {
        return allUsers().any { it.userName == username }
    }

    override fun findByToken(sessionToken: String): Boolean {
        return allUsers().any { it.sessionToken == sessionToken }
    }

    override fun add(user: User) {
        ensureStoragePathExists()
        val file = File("$storagePath/${user.id.toInt()}.json")
        file.createNewFile()
        val snapshot = user.snapshot()
        val json = gson().toJson(snapshot)
        file.writeText(json, Charsets.UTF_8)
    }

    private fun ensureStoragePathExists() {
        val storage = File(storagePath)
        if (storage.exists()) return
        storage.mkdirs()
    }

    override fun nextId(): UserId {
        val maxId = allUsers().maxOfOrNull { it.id.toInt() } ?: 0
        return UserId(maxId + 1)
    }

    private fun gson(): Gson {
       return GsonBuilder()
           .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeSerializer())
           .create()
    }

    private fun allUsers(): Sequence<User> {
        ensureStoragePathExists()
        return File(storagePath).walk().filter { it.isFile }.map {
            val id = it.nameWithoutExtension.toInt()
            getById(UserId(id))
        }
    }
}