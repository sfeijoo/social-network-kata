package core.infrastructure

import core.domain.FollowingRepository
import core.domain.PostRepository
import core.domain.UserRepository
import core.useCases.ShowWall
import core.useCases.auth.Login
import core.useCases.auth.Signup
import core.useCases.follow.Follow
import core.useCases.follow.Unfollow
import core.useCases.post.PostMessage
import core.useCases.post.Read

class UseCaseProvider(private val config: CoreConfiguration) {
    private val userRepository = config.repositoryFactory.get(UserRepository::class)
    private val postRepository = config.repositoryFactory.get(PostRepository::class)
    private val followingRepository = config.repositoryFactory.get(FollowingRepository::class)

    fun login() = Login(userRepository, config.sessionTokenGenerator)
    fun signup() = Signup(userRepository)
    fun postMessage() = PostMessage(userRepository, postRepository, config.clock)
    fun read() = Read(userRepository, postRepository)
    fun follow() = Follow(userRepository, followingRepository)
    fun unfollow() = Unfollow(userRepository, followingRepository)
    fun showWall() = ShowWall(userRepository, postRepository, followingRepository)
}