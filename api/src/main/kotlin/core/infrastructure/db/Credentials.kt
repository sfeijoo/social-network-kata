package core.infrastructure.db

data class Credentials(val url: String, val user: String, val password: String)
