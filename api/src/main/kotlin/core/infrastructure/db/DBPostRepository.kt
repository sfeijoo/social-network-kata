package core.infrastructure.db

import com.nbottarini.asimov.environment.Env
import core.domain.PostRepository
import core.domain.post.Post
import core.domain.post.PostId
import core.domain.user.UserId
import core.infrastructure.db.generated.Sequences.POSTS_ID_SEQ
import core.infrastructure.db.generated.Tables.POSTS
import core.infrastructure.db.generated.tables.records.PostsRecord
import org.jooq.impl.DSL

class DBPostRepository(private val credentials: Credentials): PostRepository {
    override fun add(post: Post) {
        val snapshot = post.snapshot()
        context().insertInto(POSTS, POSTS.ID, POSTS.USER_ID, POSTS.MESSAGE, POSTS.CREATED_AT)
            .values(snapshot.id, snapshot.userId, snapshot.message, snapshot.createdAt)
            .execute()
    }

    override fun nextId(): PostId {
        return PostId(context().nextval(POSTS_ID_SEQ).toInt())
    }

    override fun get(postId: PostId): Post {
        return context().selectFrom(POSTS)
            .where(POSTS.ID.eq(postId.toInt()))
            .fetchOne { fromRecord(it) }!!
    }

    private fun fromRecord(record: PostsRecord): Post {
        val snapshot = Post.PostSnapshot(
            record.id,
            record.userId,
            record.message,
            record.createdAt,
        )
        return Post.from(snapshot)
    }

    override fun getAllPosts() = context().selectFrom(POSTS).fetch { fromRecord(it) }

    override fun getByUserId(userId: UserId): List<Post> {
        return context().selectFrom(POSTS)
            .where(POSTS.USER_ID.eq(userId.toInt()))
            .fetch { fromRecord(it) }
    }

    private fun context() = DSL.using(credentials.url, credentials.user, credentials.password)
}
