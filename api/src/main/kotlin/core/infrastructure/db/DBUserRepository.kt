package core.infrastructure.db

import com.nbottarini.asimov.environment.Env
import core.domain.UserRepository
import core.domain.user.User
import core.domain.user.UserId
import core.infrastructure.db.generated.Sequences.USERS_ID_SEQ
import core.infrastructure.db.generated.Tables.USERS
import core.infrastructure.db.generated.tables.records.UsersRecord
import org.jooq.impl.DSL

class DBUserRepository(private val credentials: Credentials): UserRepository {
    override fun get(userName: String): User {
        return context().selectFrom(USERS)
            .where(USERS.USERNAME.eq(userName))
            .fetchOne { fromRecord(it) }!!
    }

    override fun getBySessionToken(token: String): User {
        return context().selectFrom(USERS)
            .where(USERS.SESSION_TOKEN.eq(token))
            .fetchOne { fromRecord(it) }!!
    }

    override fun getById(userId: UserId): User {
        return context().selectFrom(USERS)
            .where(USERS.ID.eq(userId.toInt()))
            .fetchOne { fromRecord(it) }!!
    }

    private fun fromRecord(record: UsersRecord): User {
        val snapshot = User.UserSnapshot(
            record.id,
            record.username,
            record.password,
            record.sessionToken,
            record.createdAt,
        )
        return User.from(snapshot)
    }

    override fun find(username: String): Boolean {
        return context().selectFrom(USERS)
            .where(USERS.USERNAME.eq(username))
            .fetchOne() != null
    }

    override fun findByToken(sessionToken: String): Boolean {
        return context().selectFrom(USERS)
            .where(USERS.SESSION_TOKEN.eq(sessionToken))
            .fetchOne() != null
    }

    override fun add(user: User) {
        val snapshot = user.snapshot()
        context().insertInto(USERS, USERS.ID, USERS.USERNAME, USERS.PASSWORD, USERS.SESSION_TOKEN, USERS.CREATED_AT)
            .values(snapshot.id, snapshot.userName, snapshot.password, snapshot.sessionToken, snapshot.createdAt)
            .execute()
    }

    override fun nextId(): UserId {
        return UserId(context().nextval(USERS_ID_SEQ))
    }

    private fun context() = DSL.using(credentials.url, credentials.user, credentials.password)
}
