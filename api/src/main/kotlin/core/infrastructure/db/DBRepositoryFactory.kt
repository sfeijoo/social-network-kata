@file:Suppress("UNCHECKED_CAST")

package core.infrastructure.db

import core.domain.FollowingRepository
import core.domain.PostRepository
import core.domain.RepositoryFactory
import core.domain.UserRepository
import core.infrastructure.inMemory.InMemoryFollowingRepository
import core.infrastructure.inMemory.InMemoryPostRepository
import core.infrastructure.inMemory.InMemoryUserRepository
import kotlin.reflect.KClass

class DBRepositoryFactory(private val credentials: Credentials): RepositoryFactory {
    private val cache = mutableMapOf<KClass<*>, Any>()

    override fun <T: Any> get(repositoryType: KClass<T>): T {
        return cache.getOrPut(repositoryType) { create(repositoryType) } as T
    }

    private fun <T: Any> create(repositoryType: KClass<T>): T {
        return when(repositoryType) {
            UserRepository::class -> DBUserRepository(credentials)
            PostRepository::class -> DBPostRepository(credentials)
            FollowingRepository::class -> InMemoryFollowingRepository()
            else -> throw NotImplementedError()
        } as T
    }

}