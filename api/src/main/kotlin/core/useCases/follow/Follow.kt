package core.useCases.follow

import core.domain.FollowingRepository
import core.domain.UserRepository
import core.domain.following.Following
import core.domain.following.FollowingId
import core.domain.user.UserNotFoundError
import core.useCases.UnauthenticatedUserError

class Follow(private val userRepository: UserRepository, private val followingRepository: FollowingRepository) {
    fun execute(userName: String, sessionToken: String): FollowingId {
        failIfNotAuthenticated(sessionToken)
        failIfNonexistentUser(userName)

        val user = userRepository.getBySessionToken(sessionToken)
        val userToFollow = userRepository.get(userName)

        val followingId = followingRepository.nextId()
        val following = Following(followingId, user.id, userToFollow.id)

        followingRepository.add(following)

        return followingId
    }

    private fun failIfNonexistentUser(userName: String) {
        if (!userRepository.find(userName)) throw UserNotFoundError()
    }

    private fun failIfNotAuthenticated(sessionToken: String) {
        if (!userRepository.findByToken(sessionToken)) throw UnauthenticatedUserError()
    }
}