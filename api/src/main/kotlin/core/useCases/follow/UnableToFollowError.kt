package core.useCases.follow

class UnableToFollowError (message: String) : Throwable(message) {
}
