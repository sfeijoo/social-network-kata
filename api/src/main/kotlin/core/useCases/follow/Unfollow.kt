package core.useCases.follow

import core.domain.FollowingRepository
import core.domain.UserRepository
import core.domain.following.FollowingId
import core.useCases.UnauthenticatedUserError

class Unfollow(private val userRepository: UserRepository, private val followingRepository: FollowingRepository) {
    fun execute(followingId: Int, sessionToken: String) {
        failIfNotAuthenticated(sessionToken)

        val following = followingRepository.get(FollowingId(followingId))

        followingRepository.remove(following)
    }

    private fun failIfNotAuthenticated(sessionToken: String) {
        if (!userRepository.findByToken(sessionToken)) throw UnauthenticatedUserError()
    }
}
