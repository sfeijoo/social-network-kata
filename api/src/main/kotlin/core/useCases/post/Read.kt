package core.useCases.post

import core.domain.PostRepository
import core.domain.UserRepository
import core.domain.post.Post
import core.domain.user.UserNotFoundError
import core.useCases.UnauthenticatedUserError

class Read (private val userRepository: UserRepository, private val postRepository: PostRepository) {
    fun execute(username: String, token: String?): List<PostData> {
        verifyCommand(username, token)
        val posts = postRepository.getAllPosts()
        return getPostData(posts)
    }

    private fun getPostData(posts: List<Post>): List<PostData> {
        var timeline = mutableListOf<PostData>()
        for (post in posts){
            timeline.add(PostData(post.id, post.message, post.date))
        }
        return timeline
    }

    private fun verifyCommand(username: String, token: String?) {
        verifyUserExists(username)
        verifyAuthenticatedUser(token)
    }

    private fun verifyUserExists(user: String) {
        userRepository.get(user)
    }

    private fun verifyAuthenticatedUser(token: String?) {
        try {
            userRepository.getBySessionToken(token!!)
        } catch (e: UserNotFoundError) {
            throw UnauthenticatedUserError()
        }
    }
}
