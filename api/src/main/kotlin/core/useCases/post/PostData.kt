package core.useCases.post

import core.domain.post.PostId
import java.time.LocalDateTime

data class PostData(val id: PostId, val message: String, val date: LocalDateTime)