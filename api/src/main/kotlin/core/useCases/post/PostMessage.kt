package core.useCases.post

import core.domain.*
import core.domain.post.Post
import core.domain.post.PostId
import core.domain.user.User
import core.domain.user.UserNotFoundError
import core.useCases.Clock
import core.useCases.UnauthenticatedUserError

class PostMessage(private val userRepository: UserRepository, private val postRepository: PostRepository, private val dateCalculator: Clock) {
    fun execute(message: String, token: String?): PostId {
        validate(message)
        val user = getUser(token)
        val post = createPost(user, message)
        return post.id
    }

    private fun createPost(user: User, message: String): Post {
        val postId = postRepository.nextId()
        return Post(postId, user.id, message, dateCalculator.now()).also { postRepository.add(it) }
    }

    private fun getUser(token: String?): User {
        try {
            return userRepository.getBySessionToken(token!!)
        } catch (e: UserNotFoundError) {
            throw UnauthenticatedUserError()
        }
    }

    private fun validate(message: String) {
        if (message.isEmpty()) throw UnableToPostError("Empty post")
    }
}
