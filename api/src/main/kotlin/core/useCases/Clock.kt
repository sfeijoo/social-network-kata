package core.useCases

import java.time.LocalDateTime

interface Clock {
    fun now(): LocalDateTime
}