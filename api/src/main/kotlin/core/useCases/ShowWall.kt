package core.useCases

import core.domain.FollowingRepository
import core.domain.PostRepository
import core.domain.UserRepository
import core.domain.WallItem
import core.domain.following.Following

class ShowWall(private val userRepository: UserRepository, private val postRepository: PostRepository, private val followingRepository: FollowingRepository) {
    fun execute(sessionToken: String): List<WallItem> {
        val user = userRepository.getBySessionToken(sessionToken)
        val followings = followingRepository.findByUser(user.id)
        val wall = createWall(followings)

        return wall.sortedBy { it.date }.take(50)
    }

    private fun createWall(followings: List<Following>): MutableList<WallItem> {
        val wall = mutableListOf<WallItem>()

        for (following in followings) {
            val posts = postRepository.getByUserId(following.followsId)
            for (post in posts) {
                val username = userRepository.getById(post.userId).userName
                val wallItem = WallItem(post.id, post.userId, username, post.message, post.date)
                wall.add(wallItem)
            }
        }
        return wall
    }
}
