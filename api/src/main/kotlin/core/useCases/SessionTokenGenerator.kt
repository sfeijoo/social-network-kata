package core.useCases

interface SessionTokenGenerator {
    fun create(): String
}