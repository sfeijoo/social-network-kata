package core.useCases.auth

import core.domain.user.User
import core.domain.UserRepository
import core.domain.user.UserId

class Signup(private val userRepository: UserRepository) {
    fun execute(userName: String, password: String): UserId {
        val userId = userRepository.nextId()
        userRepository.add(User(userId, userName, password))
        return userId
    }
}