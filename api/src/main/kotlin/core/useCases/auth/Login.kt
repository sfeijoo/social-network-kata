package core.useCases.auth

import core.domain.user.UserNotFoundError
import core.domain.UserRepository
import core.useCases.SessionTokenGenerator

class Login(private val userRepository: UserRepository, private val sessionToken: SessionTokenGenerator) {
    fun execute(userName: String, password: String): Response {
        return try {
            val user = userRepository.get(userName)
            val userSessionToken = sessionToken.create()
            val isSuccessful = password == user.password
            user.sessionToken = userSessionToken
            Response(isSuccessful, userSessionToken)
        } catch (e: UserNotFoundError){
            Response(false, null)
        }
    }

    data class Response(val isSuccessful: Boolean, val sessionToken: String?)
}
