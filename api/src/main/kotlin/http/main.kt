package http

import com.nbottarini.asimov.environment.Env
import core.infrastructure.CoreConfiguration
import core.infrastructure.time.SystemClock
import core.infrastructure.UseCaseProvider
import core.infrastructure.db.Credentials
import core.infrastructure.db.DBRepositoryFactory
import core.infrastructure.fileSystem.FileSystemRepositoryFactory
import core.infrastructure.fileSystem.FileSystemUserRepository

fun main() {
    val clock = SystemClock()
    val credentials = Credentials(Env["DB_URL"]!!, Env["DB_USER"]!!, Env["DB_PASSWORD"]!!)
    val coreConfig = CoreConfiguration(clock, repositoryFactory = DBRepositoryFactory(credentials))
    val config = HttpApplicationConfiguration(6060, UseCaseProvider(coreConfig))
    HttpApplication(config).start()
}
