package http.controllers

import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import core.domain.WallItem
import core.domain.post.Post
import core.domain.user.User
import core.infrastructure.UseCaseProvider
import core.useCases.post.PostData
import io.javalin.Javalin
import io.javalin.http.Context
import java.util.*
import kotlin.collections.HashMap

class WallController (httpServer: Javalin, private val useCaseProvider: UseCaseProvider) {
    init {
        httpServer.get("users/{userId}/wall", ::wall)
    }

    private fun wall(ctx: Context) {
        val sessionToken = getSessionToken(ctx) ?: ""

        val response = useCaseProvider.showWall().execute(sessionToken)

        ctx.contentType("application/json")
        ctx.result(createJson(response))
    }

    private fun createJson(posts: List<WallItem>): String {
        val array = JsonArray()
        for (post in posts) {
            val postJson = JsonObject()
                .add("username", post.userName)
                .add("message", post.message)
                .add("date", post.date.toString())
            array.add(postJson)
        }
        return JsonObject().add("posts", array).toString()
    }

    private fun getSessionToken(ctx: Context) = ctx.header("Authorization")?.split(" ")?.get(1)
}