package http.controllers

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import core.infrastructure.UseCaseProvider
import core.useCases.post.PostData
import io.javalin.Javalin
import io.javalin.http.Context

class FollowController(httpServer: Javalin, private val useCaseProvider: UseCaseProvider) {
    init {
        httpServer.post("/users/{userId}/followings", ::follow)
        httpServer.delete("/users/{userId}/followings/{followingId}", ::unfollow)
    }

    private fun follow(ctx: Context) {
        val json = Json.parse(ctx.body()).asObject()
        val username = json["username"].asString()
        val sessionToken = getSessionToken(ctx) ?: ""

        useCaseProvider.follow().execute(username, sessionToken)
    }

    private fun unfollow(ctx: Context) {
        val sessionToken = getSessionToken(ctx) ?: ""
        val followingId = ctx.pathParam("followingId").toInt()

        useCaseProvider.unfollow().execute(followingId, sessionToken)
    }

    private fun getSessionToken(ctx: Context) = ctx.header("Authorization")?.split(" ")?.get(1)
}