package http.controllers

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonObject
import core.infrastructure.UseCaseProvider
import core.useCases.auth.Login
import io.javalin.Javalin
import io.javalin.http.Context

class AuthController(httpServer: Javalin, private val useCaseProvider: UseCaseProvider) {
    init {
        httpServer.post("/login", ::login)
        httpServer.post("/users", ::createUser)
    }

    private fun createUser(ctx: Context) {
        val json = Json.parse(ctx.body()).asObject()
        val userName = json["username"].asString()
        val password = json["password"].asString()

        val response = useCaseProvider.signup().execute(userName, password)
        ctx.contentType("application/json")
        ctx.result(JsonObject().add("userId", response.toInt()).toString())
    }

    private fun login(ctx: Context) {
        val json = Json.parse(ctx.body()).asObject()
        val userName = json["username"].asString()
        val password = json["password"].asString()

        val response = useCaseProvider.login().execute(userName, password)

        sendResponse(ctx, response)
    }

    private fun sendResponse(ctx: Context, response: Login.Response) {
        ctx.contentType("application/json")
        ctx.result(JsonObject()
            .add("isSuccessful", response.isSuccessful)
            .add("sessionToken", response.sessionToken)
            .toString()
        )
    }
}
