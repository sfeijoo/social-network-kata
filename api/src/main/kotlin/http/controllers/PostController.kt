package http.controllers

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import core.infrastructure.UseCaseProvider
import core.useCases.post.PostData
import io.javalin.Javalin
import io.javalin.http.Context

class PostController(httpServer: Javalin, private val useCaseProvider: UseCaseProvider) {
    init {
        httpServer.post("/posts", ::post)
        httpServer.get("/posts/search", ::read)
    }

    private fun post(ctx: Context) {
        val json = Json.parse(ctx.body()).asObject()
        val message = json["message"].asString()
        val sessionToken = getSessionToken(ctx)

        if (sessionTokenIsEmpty(sessionToken, ctx)) return

        val response = useCaseProvider.postMessage().execute(message, sessionToken)

        ctx.contentType("application/json")
        ctx.result(JsonObject().add("postId", response.toInt()).toString())
    }

    private fun read(ctx: Context) {
        val userName = ctx.queryParam("username") ?: ""
        val sessionToken = getSessionToken(ctx)

        if (sessionTokenIsEmpty(sessionToken, ctx)) return

        val response = useCaseProvider.read().execute(userName, sessionToken)

        ctx.contentType("application/json")
        ctx.result(createJson(response))
    }

    private fun sessionTokenIsEmpty(sessionToken: String?, ctx: Context): Boolean {
        if (sessionToken == null) {
            ctx.status(401)
            return true
        }
        return false
    }


    private fun getSessionToken(ctx: Context) = ctx.header("Authorization")?.split(" ")?.get(1)

    private fun createJson(posts: List<PostData>): String {
        val array = JsonArray()
        for (post in posts) {
            val postJson = JsonObject()
                .add("id", post.id.toInt())
                .add("message", post.message)
                .add("date", post.date.toString())
            array.add(postJson)
        }
        return JsonObject().add("posts", array).toString()
    }
}