package http

import core.infrastructure.UseCaseProvider

data class HttpApplicationConfiguration(val port: Int, val useCaseProvider: UseCaseProvider)