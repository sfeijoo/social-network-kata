package http

import com.eclipsesource.json.JsonObject
import core.domain.DomainError
import http.controllers.AuthController
import http.controllers.FollowController
import http.controllers.PostController
import http.controllers.WallController
import io.javalin.Javalin
import io.javalin.http.Context

class HttpApplication(private val config: HttpApplicationConfiguration) {
    private val httpServer = Javalin.create {
        it.enableCorsForAllOrigins()
    }

    init {
        registerControllers()
        registerKnownErrors()
    }

    private fun registerKnownErrors() {
        httpServer.exception(DomainError::class.java, ::badRequestErrorHandler)
    }

    private fun badRequestErrorHandler(error: DomainError, context: Context) {
        context.contentType("application/json")
        context.status(400)
        context.result(JsonObject().add("type", error.javaClass.simpleName).add("message", error.message).toString())
    }

    private fun registerControllers() {
        AuthController(httpServer, config.useCaseProvider)
        PostController(httpServer,config.useCaseProvider)
        FollowController(httpServer, config.useCaseProvider)
        WallController(httpServer, config.useCaseProvider)
    }

    fun start() {
        httpServer.start(config.port)
    }
    fun stop() {
        httpServer.stop()
    }
}
