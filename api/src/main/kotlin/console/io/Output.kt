package console.io

interface Output {
    fun println(message: String)
    fun print(message: String)

}
