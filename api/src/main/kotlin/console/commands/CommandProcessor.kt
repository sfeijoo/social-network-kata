package console.commands

import console.io.Input
import console.io.Output

class CommandProcessor(
    private val input: Input,
    private val output: Output,
    private val commandFactory: CommandHandlerFactory,
    private val promptPrinter: PromptPrinter
) {
    fun run() {
        while (true) {
            val command = readCommand()
            if (command.name == "exit") break
            process(command)
        }
        output.println("Bye")
    }

    private fun process(command: Command) {
        try {
            val commandOutput = commandFactory.create(command.name).handle(command)
            if (!commandOutput.isNullOrBlank()) output.println(commandOutput)
        } catch (e: InvalidCommandError) {
            output.println(e.message!!)
        }
    }

    private fun readCommand(): Command {
        promptPrinter.print()
        val commandText = input.readLine()
        val parts = commandText.split(" ")
        return Command(name = parts[0], parameters = parts.drop(1))
    }
}