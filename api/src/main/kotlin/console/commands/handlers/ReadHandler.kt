package console.commands.handlers

import console.formatters.RelativeTimeFormatter
import console.Session
import console.commands.Command
import core.domain.user.UserNotFoundError
import core.useCases.Clock
import core.useCases.post.PostData
import core.useCases.post.Read
import core.useCases.UnauthenticatedUserError

class ReadHandler(private val read: Read, private val session: Session, private val clock: Clock): CommandHandler {
    private val relativeTimeFormatter = RelativeTimeFormatter(clock)
    override val name: String
        get() = "read"

    override fun handle(command: Command): String {
        val user = command.parameters[0]
        return try {
            tryRead(user)
        } catch (e: UserNotFoundError) {
            "ERROR: Unknown user $user"
        } catch (e: UnauthenticatedUserError) {
            "ERROR: User must be logged in"
        }
    }

    private fun tryRead(user: String): String {
        val posts = read.execute(user, session.token)
        return format(posts)
    }

    private fun format(posts: List<PostData>): String {
        return posts.joinToString("") {
            "- ${it.message} (${relativeTimeFormatter.format(it.date)})\n"
        }.trim()
    }
}