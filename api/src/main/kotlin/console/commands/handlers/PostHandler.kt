package console.commands.handlers

import console.Session
import console.commands.Command
import core.useCases.post.UnableToPostError
import core.useCases.UnauthenticatedUserError
import core.useCases.post.PostMessage

class PostHandler(private val post: PostMessage, private val session: Session) : CommandHandler {
    override val name: String
        get() = "post"

    override fun handle(command: Command): String {
        return try {
            tryPost(command)
            ""
        } catch (e: UnableToPostError) {
            "ERROR: ${e.message}"
        } catch (e: UnauthenticatedUserError) {
            "ERROR: User must be logged"
        }
    }

    private fun tryPost(command: Command) {
        val message = getMessage(command.parameters)
        post.execute(message, session.token)
    }

    private fun getMessage(parameters: List<String>) = parameters.joinToString(" ")
}
