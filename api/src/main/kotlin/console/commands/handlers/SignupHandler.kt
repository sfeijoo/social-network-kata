package console.commands.handlers

import console.commands.Command
import core.domain.user.UserAlreadyExistsError
import core.useCases.auth.Signup

class SignupHandler(private val signup: Signup): CommandHandler {
    override val name: String
        get() = "signup"

    override fun handle(command: Command): String {
        val userName = command.parameters[0]
        val password = command.parameters[1]

        return try {
            signup.execute(userName, password)
            "Signup successful"
        } catch (e: UserAlreadyExistsError) {
            "ERROR: User $userName already exists"
        }
    }

}
