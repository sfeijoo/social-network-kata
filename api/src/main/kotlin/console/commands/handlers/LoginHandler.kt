package console.commands.handlers

import console.Session
import console.commands.Command
import core.useCases.auth.Login

class LoginHandler(private val login: Login, private val session: Session) : CommandHandler {
    override val name: String
        get() = "login"

    override fun handle(command: Command): String {
        val userName = command.parameters[0]
        val password = command.parameters[1]
        val response = login.execute(userName, password)

        if (!response.isSuccessful) return "Login fail"

        session.authenticate(userName, response.sessionToken!!)
        return "Login successful"
    }
}
