package console.commands.handlers

import console.commands.Command

interface CommandHandler {
    val name: String
    fun handle(command: Command): String
}