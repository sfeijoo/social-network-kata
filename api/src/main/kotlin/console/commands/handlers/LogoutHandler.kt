package console.commands.handlers

import console.Session
import console.commands.Command

class LogoutHandler(private val session: Session): CommandHandler {
    override val name: String
        get() = "logout"

    override fun handle(command: Command): String {
        if (!session.isAuthenticated) return ""
        session.logout()
        return "Logged out"
    }
}
