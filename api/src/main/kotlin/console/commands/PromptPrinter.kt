package console.commands

import console.Session
import console.io.Output

class PromptPrinter(private val output: Output, private val session: Session) {
    fun print() {
        val user = session.user ?: ""
        output.print("$user> ")
    }
}
