package console.commands

import console.commands.handlers.CommandHandler

class CommandHandlerFactory {
    private val handlers = mutableMapOf<String, CommandHandler>()

    fun register(handler: CommandHandler) {
        handlers[handler.name] = handler
    }

    fun create(command: String): CommandHandler {
        return handlers[command] ?: throw InvalidCommandError(command)
    }
}