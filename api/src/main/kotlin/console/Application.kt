package console

import console.commands.*
import console.commands.handlers.*

class Application(config: ApplicationConfiguration) {
    private val session = Session()
    private val commandFactory = CommandHandlerFactory()
    private val promptPrinter = PromptPrinter(config.output, session)
    private val commandProcessor = CommandProcessor(config.input, config.output, commandFactory, promptPrinter)

    init {
        with(commandFactory) {
            register(LoginHandler(config.useCaseProvider.login(), session))
            register(SignupHandler(config.useCaseProvider.signup()))
            register(LogoutHandler(session))
            register(PostHandler(config.useCaseProvider.postMessage(), session))
            register(ReadHandler(config.useCaseProvider.read(), session, config.clock))
        }
    }

    fun run() {
        commandProcessor.run()
    }

}
