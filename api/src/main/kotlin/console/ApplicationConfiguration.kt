package console

import console.io.Input
import console.io.Output
import core.infrastructure.UseCaseProvider
import core.useCases.Clock

data class ApplicationConfiguration(
    val clock: Clock,
    val useCaseProvider: UseCaseProvider,
    val input: Input,
    val output: Output
)
