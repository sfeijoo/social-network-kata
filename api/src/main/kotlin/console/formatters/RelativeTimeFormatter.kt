package console.formatters

import core.useCases.Clock
import java.time.LocalDateTime
import java.time.temporal.ChronoField.*
import java.time.temporal.ChronoUnit
import kotlin.math.abs

class RelativeTimeFormatter(private val clock: Clock) {
    private val minutesInAnHour = 60L
    private val minutesInADay = 24L * minutesInAnHour
    private val minutesInAMonth = 30L * minutesInADay
    private val minutesInAYear = 12 * minutesInAMonth

    fun format(date: LocalDateTime): String {
        val minutesAgo = date.minutesTo(clock.now())

        return when {
            minutesAgo == 0L -> "just now"
            minutesAgo < minutesInAnHour -> "$minutesAgo minute(s) ago"
            minutesAgo < minutesInADay -> "${minutesAgo / minutesInAnHour } hour(s) ago"
            minutesAgo < minutesInAMonth -> "${minutesAgo / minutesInADay } day(s) ago"
            minutesAgo < minutesInAYear -> "${minutesAgo / minutesInAMonth } month(s) ago"
            else -> "${minutesAgo / minutesInAYear } year(s) ago"
        }
    }

    private fun LocalDateTime.minutesTo(other: LocalDateTime) = abs(ChronoUnit.MINUTES.between(this, other))
}
