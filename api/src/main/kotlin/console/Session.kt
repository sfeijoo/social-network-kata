package console

class Session {
    var token: String? = null
        private set
    var user: String? = null
        private set
    val isAuthenticated get() = user != null

    fun authenticate(user: String, token: String) {
        this.user = user
        this.token = token
    }

    fun logout() {
        user = null
        token = null
    }
}
