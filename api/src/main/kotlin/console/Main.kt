import console.ApplicationConfiguration
import console.Application
import console.io.SystemInput
import console.io.SystemOutput
import core.infrastructure.CoreConfiguration
import core.infrastructure.time.SystemClock
import core.infrastructure.UseCaseProvider

fun main() {
    val clock = SystemClock()
    val config = ApplicationConfiguration(
        clock,
        UseCaseProvider(CoreConfiguration(clock)),
        SystemInput(),
        SystemOutput()
    )
    val console = Application(config)
    console.run()
}