/*
 * This file is generated by jOOQ.
 */
package core.infrastructure.db.generated.tables.records;


import core.infrastructure.db.generated.tables.Posts;

import java.time.LocalDateTime;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class PostsRecord extends UpdatableRecordImpl<PostsRecord> implements Record4<Integer, Integer, String, LocalDateTime> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>public.posts.id</code>.
     */
    public PostsRecord setId(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.posts.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.posts.user_id</code>.
     */
    public PostsRecord setUserId(Integer value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.posts.user_id</code>.
     */
    public Integer getUserId() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>public.posts.message</code>.
     */
    public PostsRecord setMessage(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.posts.message</code>.
     */
    public String getMessage() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.posts.created_at</code>.
     */
    public PostsRecord setCreatedAt(LocalDateTime value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>public.posts.created_at</code>.
     */
    public LocalDateTime getCreatedAt() {
        return (LocalDateTime) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<Integer, Integer, String, LocalDateTime> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<Integer, Integer, String, LocalDateTime> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return Posts.POSTS.ID;
    }

    @Override
    public Field<Integer> field2() {
        return Posts.POSTS.USER_ID;
    }

    @Override
    public Field<String> field3() {
        return Posts.POSTS.MESSAGE;
    }

    @Override
    public Field<LocalDateTime> field4() {
        return Posts.POSTS.CREATED_AT;
    }

    @Override
    public Integer component1() {
        return getId();
    }

    @Override
    public Integer component2() {
        return getUserId();
    }

    @Override
    public String component3() {
        return getMessage();
    }

    @Override
    public LocalDateTime component4() {
        return getCreatedAt();
    }

    @Override
    public Integer value1() {
        return getId();
    }

    @Override
    public Integer value2() {
        return getUserId();
    }

    @Override
    public String value3() {
        return getMessage();
    }

    @Override
    public LocalDateTime value4() {
        return getCreatedAt();
    }

    @Override
    public PostsRecord value1(Integer value) {
        setId(value);
        return this;
    }

    @Override
    public PostsRecord value2(Integer value) {
        setUserId(value);
        return this;
    }

    @Override
    public PostsRecord value3(String value) {
        setMessage(value);
        return this;
    }

    @Override
    public PostsRecord value4(LocalDateTime value) {
        setCreatedAt(value);
        return this;
    }

    @Override
    public PostsRecord values(Integer value1, Integer value2, String value3, LocalDateTime value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached PostsRecord
     */
    public PostsRecord() {
        super(Posts.POSTS);
    }

    /**
     * Create a detached, initialised PostsRecord
     */
    public PostsRecord(Integer id, Integer userId, String message, LocalDateTime createdAt) {
        super(Posts.POSTS);

        setId(id);
        setUserId(userId);
        setMessage(message);
        setCreatedAt(createdAt);
    }

    /**
     * Create a detached, initialised PostsRecord
     */
    public PostsRecord(core.infrastructure.db.generated.tables.pojos.Posts value) {
        super(Posts.POSTS);

        if (value != null) {
            setId(value.getId());
            setUserId(value.getUserId());
            setMessage(value.getMessage());
            setCreatedAt(value.getCreatedAt());
        }
    }
}
