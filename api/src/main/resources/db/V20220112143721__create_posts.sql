CREATE TABLE posts(
    id             SERIAL PRIMARY KEY,
    user_id        INTEGER NOT NULL REFERENCES users(id),
    message        VARCHAR(50) NOT NULL,
    created_at     TIMESTAMP NOT NULL
);