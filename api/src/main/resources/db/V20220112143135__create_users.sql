CREATE TABLE users(
    id              SERIAL PRIMARY KEY,
    username        VARCHAR(50) NOT NULL,
    password        VARCHAR(50) NOT NULL,
    session_token   VARCHAR(500) NULL,
    created_at      TIMESTAMP
);

CREATE UNIQUE INDEX users_username_uq ON users(LOWER(username));