package console.formatters

import core.domain.DateExamples.now
import core.useCases.StoppedClock
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class RelativeTimeFormatterShould {
    @Test
    fun `return message when difference is in seconds`() {
        val output = formatter.format(now)

        assertThat(output).isEqualTo("just now")
    }
    @Test
    fun `return time difference in minutes`() {
        val output = formatter.format(now.minusMinutes(15))

        assertThat(output).isEqualTo("15 minute(s) ago")
    }

    @Test
    fun `return time difference in hours`() {
        val output = formatter.format(now.minusHours(4))

        assertThat(output).isEqualTo("4 hour(s) ago")
    }

    @Test
    fun `return time difference in days`() {
        val output = formatter.format(now.minusDays(2))

        assertThat(output).isEqualTo("2 day(s) ago")
    }

    @Test
    fun `return time difference in months`() {
        val output = formatter.format(now.minusMonths(4))

        assertThat(output).isEqualTo("4 month(s) ago")
    }

    @Test
    fun `return time difference in years`() {
        assertThat(formatter.format(now.minusYears(2))).isEqualTo("2 year(s) ago")
        assertThat(formatter.format(now.minusYears(3))).isEqualTo("3 year(s) ago")
    }

    private val clock = StoppedClock.at(now)
    private val formatter = RelativeTimeFormatter(clock)

}