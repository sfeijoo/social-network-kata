package console.commands.handlers

import console.SessionExamples.aliceSession
import console.commands.Command
import core.domain.DateExamples.now
import core.domain.UserExamples.alice
import core.domain.post.PostId
import core.domain.user.UserNotFoundError
import core.useCases.StoppedClock
import core.useCases.post.PostData
import core.useCases.post.Read
import core.useCases.UnauthenticatedUserError
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class ReadHandlerShould {
    @Test
    fun `return posts from given user`() {
        every { read.execute(any(), any()) } returns listOf(
            post("First message", now.minusDays(1)),
            post("Second message", now)
        )

        val response = readHandler.handle(Command("read", listOf(alice)))

        assertThat(response).isEqualTo(
            "- First message (1 day(s) ago)\n" +
            "- Second message (just now)"
        )
    }

    @Test
    fun `return fail message if requested user does not exist`() {
        every { read.execute(any(), any()) } throws UserNotFoundError()

        val output = readHandler.handle(aliceReadCommand)

        assertThat(output).isEqualTo("ERROR: Unknown user $alice")
    }

    @Test
    fun `return fail message if not logged in`() {
        every { read.execute(any(), any()) } throws UnauthenticatedUserError()

        val output = readHandler.handle(aliceReadCommand)

        assertThat(output).isEqualTo("ERROR: User must be logged in")
    }

    private fun post(message: String, date: LocalDateTime) = PostData(PostId(postNextId++), message, date)

    private val read = mockk<Read>(relaxed = true)
    private val clock = StoppedClock.at(now)
    private val readHandler = ReadHandler(read, aliceSession, clock)
    private var postNextId = 1
    private val aliceReadCommand = Command("read", listOf(alice))
}