package console.commands.handlers

import console.Session
import console.commands.Command
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.useCases.auth.Login
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class LoginHandlerShould {
    @Test
    fun `login with given username and password`() {
        loginHandler.handle(Command("login", listOf(alice, alicePassword)))

        verify { login.execute(alice, alicePassword) }
    }

    @Test
    fun `return success message when login succeeds`() {
        every { login.execute(any(), any()) } returns successResponse

        val output = loginHandler.handle(aliceLoginCommand)

        assertThat(output).isEqualTo("Login successful")
    }

    @Test
    fun `return fail message when login fails`() {
        every { login.execute(any(), any()) } returns failResponse

        val output = loginHandler.handle(aliceLoginCommand)

        assertThat(output).isEqualTo("Login fail")
    }

    @Test
    fun `create authenticated session when login succeeds`() {
        loginHandler.handle(aliceLoginCommand)

        assertThat(session.user).isEqualTo(alice)
        assertThat(session.token).isEqualTo(sessionToken)
    }

    @BeforeEach
    fun beforeEach() {
        every { login.execute(any(), any()) } returns successResponse
    }

    private val aliceLoginCommand = Command("login", listOf(alice, alicePassword))
    private val login = mockk<Login>()
    private val session = Session()
    private val loginHandler = LoginHandler(login, session)
    private val sessionToken = "token"
    private val successResponse = Login.Response(true, sessionToken)
    private val failResponse = Login.Response(false, null)

}