package console.commands.handlers

import console.Session
import console.SessionExamples.aliceSessionToken
import console.commands.Command
import core.domain.UserExamples.alice
import core.useCases.post.UnableToPostError
import core.useCases.UnauthenticatedUserError
import core.useCases.post.PostMessage
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PostHandlerShould {
    @Test
    fun `post given message as authenticated user`() {
        session.authenticate(alice, aliceSessionToken)

        postHandler.handle(Command("post", listOf("Hello", "world!")))

        verify { post.execute("Hello world!", aliceSessionToken) }
    }

    @Test
    fun `return error when post fails`() {
        every { post.execute(any(), any()) } throws UnableToPostError("Some error")

        val output = postHandler.handle(postCommand)

        assertThat(output).isEqualTo("ERROR: Some error")
    }

    @Test
    fun `return fail message when user is not authenticated`() {
        every { post.execute(any(), any()) } throws UnauthenticatedUserError()

        val output = postHandler.handle(postCommand)

        assertThat(output).isEqualTo("ERROR: User must be logged")
    }

    private val post = mockk<PostMessage>(relaxed = true)
    private val postCommand = Command("post", listOf("Hello", "world!"))
    private val session = Session()
    private val postHandler = PostHandler(post, session)
}