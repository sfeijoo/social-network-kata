package console.commands.handlers

import console.commands.Command
import core.domain.user.UserAlreadyExistsError
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.useCases.auth.Signup
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SignupHandlerShould {
    @Test
    fun `call Signup with given username and password`() {
        signupHandler.handle(Command("signup", listOf(alice, alicePassword)))

        verify { signup.execute(alice, alicePassword) }
    }

    @Test
    fun `return success message if user was created`() {
        val output = signupHandler.handle(Command("signup", listOf(alice, alicePassword)))

        assertThat(output).isEqualTo("Signup successful")

    }
    @Test
    fun `return fail message if user already exists`() {
        every { signup.execute(any(), any()) } throws UserAlreadyExistsError(alice)

        val output = signupHandler.handle(Command("signup", listOf(alice, alicePassword)))

        assertThat(output).isEqualTo("ERROR: User $alice already exists")
    }
    
    private val signup = mockk<Signup>(relaxed = true)
    private val signupHandler = SignupHandler(signup)
}