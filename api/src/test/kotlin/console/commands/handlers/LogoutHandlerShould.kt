package console.commands.handlers

import console.Session
import console.SessionExamples.aliceSessionToken
import console.commands.Command
import core.domain.UserExamples.alice
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class LogoutHandlerShould {
    @Test
    fun `return success message when logged out`() {
        session.authenticate(alice, aliceSessionToken)

        val output = logoutHandler.handle(logoutCommand)

        assertThat(output).isEqualTo("Logged out")
    }

    @Test
    fun `not return any message if user wasn't logged in`() {
        val output = logoutHandler.handle(logoutCommand)

        assertThat(output).isEqualTo("")
    }

    @Test
    fun `clear session when logout succeeds`() {
        session.authenticate(alice, aliceSessionToken)

        logoutHandler.handle(logoutCommand)

        assertThat(session.user).isNull()
        assertThat(session.token).isNull()

    }

    private val session = Session()
    private val logoutCommand = Command("logout")
    private val logoutHandler = LogoutHandler(session)
}