package console.commands

import console.Session
import core.domain.UserExamples.alice
import console.e2e.FakeOutput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PromptPrinterShould {
    @Test
    fun `not print username when user is anonymous`() {
        session.logout()

        promptPrinter.print()

        assertThat(output.content).isEqualTo("> ")
    }

    @Test
    fun `print username when user is authenticated`() {
        session.authenticate(alice, token)

        promptPrinter.print()

        assertThat(output.content).isEqualTo("$alice> ")
    }

    private val session = Session()
    private val token = "token"
    private val output = FakeOutput()
    private val promptPrinter = PromptPrinter(output, session)
}