package console

import core.domain.UserExamples.alice
import core.domain.UserExamples.bob

object SessionExamples {
    const val aliceSessionToken = "token"
    const val bobSessionToken = "anothertoken"
    val aliceSession = Session().apply { authenticate(alice, aliceSessionToken) }
    val bobSession = Session().apply { authenticate(bob, bobSessionToken) }
}