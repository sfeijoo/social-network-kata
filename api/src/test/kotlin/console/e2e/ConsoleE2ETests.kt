package console.e2e

import console.ApplicationConfiguration
import console.Application
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.infrastructure.CoreConfiguration
import core.infrastructure.UseCaseProvider
import core.useCases.StoppedClock
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class ConsoleE2ETests {

    @Test
    fun exit() {
        input.willRead("exit")

        console.run()

        assertThat(output.lines()).containsExactly(
            "> exit",
            "Bye"
        )
    }

    @Test
    fun login() {
        input.willReadAndExit(
            "signup $alice $alicePassword",
            "login $alice $alicePassword"
        )

        console.run()

        assertThat(output.lines()).startsWith(
            "> signup $alice $alicePassword",
            "Signup successful",
            "> login $alice $alicePassword",
            "Login successful",
            "$alice> exit",
        )
    }

    @Test
    fun logout() {
        input.willReadAndExit(
            "signup $alice $alicePassword",
            "login $alice $alicePassword",
            "logout"
        )

        console.run()

        assertThat(output.lines()).containsSequence(
            "Login successful",
            "$alice> logout",
            "Logged out",
            "> exit",
        )
    }

    @Test
    fun post() {
        input.willLogAlice()
        input.willReadAndExit(
            "post $message",
            "read $alice"
        )

        console.run()

        assertThat(output.lines()).containsSequence(
            "$alice> post $message",
            "$alice> read $alice",
            "- $message (just now)",
        )
    }

    private fun FakeInput.willLogAlice() {
        willRead("signup $alice $alicePassword")
        willRead("login $alice $alicePassword")
    }

    private fun FakeInput.willReadAndExit(vararg messages: String) {
        willRead(*messages)
        willRead("exit")
    }

    private fun FakeOutput.lines() = content.lines().dropLast(1)

    private val output = FakeOutput()
    private val input = FakeInput(output)
    private val clock = StoppedClock.at(LocalDateTime.of(2021, 12, 6, 20, 20))
    private val useCaseProvider = UseCaseProvider(CoreConfiguration(clock))
    private val config = ApplicationConfiguration(clock, useCaseProvider, input, output)
    private val console = Application(config)
    private val message = "Hello world!"
}
