package console.e2e

import console.io.Input
import console.io.Output

class FakeInput(private val output: Output) : Input {
    private var contentToRead = mutableListOf<String>()
    private var index = 0

    override fun readLine(): String {
        val content = contentToRead[index++]
        output.println(content)
        return content
    }

    fun willRead(vararg messages: String) {
        messages.forEach { contentToRead.add(it) }
    }

}