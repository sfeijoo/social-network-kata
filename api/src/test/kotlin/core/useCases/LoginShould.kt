package core.useCases

import console.SessionExamples.aliceSessionToken
import core.domain.user.User
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.infrastructure.inMemory.InMemoryUserRepository
import core.useCases.auth.Login
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class LoginShould {
    @Test
    fun `return true if credentials are valid`() {
        val id = userRepository.nextId()
        userRepository.add(User(id, alice, alicePassword))

        val response = login.execute(alice, alicePassword)

        assertThat(response.isSuccessful).isTrue
    }

    @Test
    fun `return false if user doesnt exist`() {
        val response = login.execute(nonExistentUser, alicePassword)

        assertThat(response.isSuccessful).isFalse
    }
    @Test
    fun `return false if password is invalid`() {
        val id = userRepository.nextId()
        userRepository.add(User(id, alice, alicePassword))

        val response = login.execute(alice, invalidPassword)

        assertThat(response.isSuccessful).isFalse
    }

    @Test
    fun `return session token`() {
        val id = userRepository.nextId()
        userRepository.add(User(id, alice, alicePassword))

        val output = login.execute(alice, alicePassword)

        assertThat(output.sessionToken).isEqualTo(aliceSessionToken)
    }

    private val nonExistentUser = "@bob"
    private val invalidPassword = "invalid"
    private val userRepository = InMemoryUserRepository()
    private val fakeSessionToken = FakeSessionTokenGenerator(aliceSessionToken)
    private val login = Login(userRepository, fakeSessionToken)
}
