package core.useCases

import console.SessionExamples.aliceSessionToken
import console.SessionExamples.bobSessionToken
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.domain.UserExamples.bob
import core.domain.UserExamples.bobPassword
import core.domain.user.User
import core.infrastructure.inMemory.InMemoryFollowingRepository
import core.infrastructure.inMemory.InMemoryUserRepository
import core.useCases.follow.Follow
import core.useCases.follow.Unfollow
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class UnfollowShould {
    @Test
    fun `make user unfollow given user`() {
        val followingId = givenAliceFollowsBob()

        unfollow.execute(followingId, aliceSessionToken)

        assertAliceUnfollowsBob()
    }

    @Test
    fun `fail if token is invalid`() {
        assertThrows<UnauthenticatedUserError> { unfollow.execute(5, "invalidSessionToken") }
    }

    @BeforeEach
    fun beforeEach() {
        createUser(alice, alicePassword, aliceSessionToken)
    }

    private fun givenAliceFollowsBob(): Int {
        createUser(bob, bobPassword, bobSessionToken)
        val id = follow.execute(bob, aliceSessionToken)
        return id.toInt()
    }

    private fun assertAliceUnfollowsBob() {
        val aliceId = userRepository.get(alice).id
        val aliceFollows = followingRepository.findByUser(aliceId)

        assertThat(aliceFollows).isEmpty()
    }

    private fun createUser(user: String, password: String, sessionToken: String): User {
        val id = userRepository.nextId()
        val user = User(id, user, password)
        user.sessionToken = sessionToken
        userRepository.add(user)
        return user
    }

    private val userRepository = InMemoryUserRepository()
    private val followingRepository = InMemoryFollowingRepository()
    private val follow = Follow(userRepository, followingRepository)
    private val unfollow = Unfollow(userRepository, followingRepository)
}