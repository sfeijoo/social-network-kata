package core.useCases

import console.SessionExamples.aliceSession
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.domain.UserExamples.bob
import core.domain.UserExamples.bobPassword
import core.domain.post.Post
import core.domain.post.PostId
import core.domain.user.User
import core.domain.user.UserId
import core.domain.user.UserNotFoundError
import core.infrastructure.inMemory.InMemoryPostRepository
import core.infrastructure.inMemory.InMemoryUserRepository
import core.useCases.post.PostData
import core.useCases.post.Read
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.LocalDateTime

class ReadShould {
    @Test
    fun `fail if requested user is unknown`() {
        val nonexistentUser = "@bob"

        assertThrows<UserNotFoundError> { read.execute(nonexistentUser, aliceSession.token) }
    }

    @Test
    fun `fail if user is not authenticated`() {
        val invalidToken = "invalid"
        createUser(bob, bobPassword)

        assertThrows<UnauthenticatedUserError> { read.execute(bob, invalidToken) }
    }

    @Test
    fun `return all posts from requested user`() {
        createUser(alice, alicePassword)
        val aliceId = userRepository.get(alice).id
        initializePostRepository(aliceId)
        val posts = listOfPosts()

        val output = read.execute(alice, aliceSession.token)

        assertThat(output).isEqualTo(posts)
    }

    private val message1 = "Hello World!"
    private val message2 = "Bye World!"
    private val userRepository = InMemoryUserRepository()
    private val postRepository = InMemoryPostRepository()
    private val read = Read(userRepository, postRepository)
    private val fakeDateTime = StoppedClock.at(LocalDateTime.of(2021, 12, 6, 20, 20))

    private fun listOfPosts() = listOf(PostData(PostId(1), message1, fakeDateTime.now()), PostData(PostId(2), message2, fakeDateTime.now()))

    private fun createUser(username: String, password: String): User {
        val id = userRepository.nextId()
        val user = User(id, username, password)
        user.sessionToken = "token"
        userRepository.add(user)
        return user
    }

    private fun initializePostRepository(id: UserId){
        postRepository.add(Post(postRepository.nextId(), id, message1, fakeDateTime.now()))
        postRepository.add(Post(postRepository.nextId(), id, message2, fakeDateTime.now()))
    }

}