package core.useCases

import core.domain.user.User
import core.domain.user.UserAlreadyExistsError
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.infrastructure.inMemory.InMemoryUserRepository
import core.useCases.auth.Signup
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class SignupShould {
    @Test
    fun `create a new user`() {
        signup.execute(alice, alicePassword)

        val newUser = userRepository.get(alice)

        assertThat(newUser.userName).isEqualTo(alice)
        assertThat(newUser.password).isEqualTo(alicePassword)
    }

    @Test
    fun `fail if user already exists`() {
        val id = userRepository.nextId()

        userRepository.add(User(id, alice, alicePassword))

        assertThrows<UserAlreadyExistsError> { signup.execute(alice, alicePassword) }
    }

    private val userRepository = InMemoryUserRepository()
    private val signup = Signup(userRepository)
}