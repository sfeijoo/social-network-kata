package core.useCases

import console.SessionExamples.aliceSessionToken
import console.SessionExamples.bobSessionToken
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.domain.UserExamples.bob
import core.domain.UserExamples.bobPassword
import core.domain.following.Following
import core.domain.post.Post
import core.domain.user.User
import core.infrastructure.inMemory.InMemoryFollowingRepository
import core.infrastructure.inMemory.InMemoryPostRepository
import core.infrastructure.inMemory.InMemoryUserRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class ShowWallShould {
    @Test
    fun `show most recent tweets from following users`() {
        givenAliceFollowsBob()

        val response = showWall.execute(aliceSessionToken)

        assertThat(response[0].userName).isEqualTo(bob)
        assertThat(response[0].message).isEqualTo(message)
    }

    private fun givenAliceFollowsBob() {
       val alice = createUser(alice, alicePassword, aliceSessionToken)

        val bob = createUser(bob, bobPassword,bobSessionToken)
        val postid = postRepository.nextId()
        val post = Post(postid, bob.id, message, stoppedClock.now())
        postRepository.add(post)

        val followingId = followingRepository.nextId()
        val following = Following(followingId, alice.id, bob.id)
        followingRepository.add(following)
    }

    private fun createUser(user: String, password: String, sessionToken: String): User {
        val id = userRepository.nextId()
        val user = User(id, user, password)
        user.sessionToken = sessionToken
        userRepository.add(user)
        return user
    }

    private val userRepository = InMemoryUserRepository()
    private val followingRepository = InMemoryFollowingRepository()
    private val postRepository = InMemoryPostRepository()
    val stoppedClock = StoppedClock.at(LocalDateTime.of(2021, 12, 6, 20, 20))
    private val showWall = ShowWall(userRepository, postRepository, followingRepository)
    private val message = "post message"
}