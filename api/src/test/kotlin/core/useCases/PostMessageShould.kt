package core.useCases

import core.domain.user.User
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.infrastructure.inMemory.InMemoryPostRepository
import core.infrastructure.inMemory.InMemoryUserRepository
import core.useCases.post.PostMessage
import core.useCases.post.UnableToPostError
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.LocalDateTime

class PostMessageShould {
    @Test
    fun `add a new post to the user timeline`() {
        val user = createUser(alice, alicePassword)
        val postId = postMessage.execute(message, user.sessionToken)

        val newPost = postRepository.get(postId)

        assertThat(newPost.message).isEqualTo(message)
    }

    @Test
    fun `add current date to post`() {
        val user = createUser(alice, alicePassword)
        val postId = postMessage.execute(message, user.sessionToken)
        val date = LocalDateTime.of(2021, 12, 6, 20, 20)

        val newPost = postRepository.get(postId)

        assertThat(newPost.date).isEqualTo(date)
    }

    @Test
    fun `fail if message is empty`() {
        val user = createUser(alice, alicePassword)

        assertThrows<UnableToPostError> { postMessage.execute("", user.sessionToken) }
    }

    @Test
    fun `fail if token is invalid`() {
        assertThrows<UnauthenticatedUserError> { postMessage.execute(message, "invalidtoken")  }
    }

    private val userRepository = InMemoryUserRepository()
    private val postRepository = InMemoryPostRepository()
    private val stoppedClock = StoppedClock.at(LocalDateTime.of(2021, 12, 6, 20, 20))
    private val postMessage = PostMessage(userRepository, postRepository, stoppedClock)
    private val message = "Hello World!"

    private fun createUser(user: String, password: String): User {
        val id = userRepository.nextId()
        val user = User(id, user, password)
        user.sessionToken = "token"
        userRepository.add(user)
        return user
    }

}