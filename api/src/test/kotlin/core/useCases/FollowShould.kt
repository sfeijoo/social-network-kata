package core.useCases

import console.SessionExamples.aliceSessionToken
import console.SessionExamples.bobSessionToken
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.domain.UserExamples.bob
import core.domain.UserExamples.bobPassword
import core.domain.user.User
import core.domain.user.UserNotFoundError
import core.infrastructure.inMemory.InMemoryFollowingRepository
import core.infrastructure.inMemory.InMemoryUserRepository
import core.useCases.follow.Follow
import core.useCases.follow.UnableToFollowError
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class FollowShould {
    @Test
    fun `make user follow given user`() {
        createUser(bob, bobPassword, bobSessionToken)

        follow.execute(bob, aliceSessionToken)

        assertAliceFollowsBob()
    }

    @Test
    fun `fail if user to follow does not exist`() {
        assertThrows<UserNotFoundError> { follow.execute(bob, aliceSessionToken) }
    }

    @Test
    fun `fail if token is invalid`() {
        assertThrows<UnauthenticatedUserError> { follow.execute(alice, "invalidSessionToken") }
    }

    @Test
    fun `fail if already follows given user`() {
        createUser(bob, bobPassword, bobSessionToken)
        follow.execute(bob, aliceSessionToken)

        assertThrows<UnableToFollowError> { follow.execute(bob, aliceSessionToken) }
    }

    @BeforeEach
    fun beforeEach() {
        createUser(alice, alicePassword, aliceSessionToken)
    }

    private fun assertAliceFollowsBob() {
        val aliceId = userRepository.get(alice).id
        val aliceFollows = followingRepository.findByUser(aliceId)
        val followee = userRepository.getById(aliceFollows[0].followsId)

        assertThat(followee.userName).isEqualTo(bob)
    }

    private fun createUser(user: String, password: String, sessionToken: String): User {
        val id = userRepository.nextId()
        val user = User(id, user, password)
        user.sessionToken = sessionToken
        userRepository.add(user)
        return user
    }

    private val userRepository = InMemoryUserRepository()
    private val followingRepository = InMemoryFollowingRepository()
    private val follow = Follow(userRepository, followingRepository)
}