package core.useCases

import console.SessionExamples.bobSessionToken

class FakeSessionTokenGenerator(private val token: String): SessionTokenGenerator {
    private var times = 0
    override fun create(): String {
        if (times > 0) return bobSessionToken
        times++
        return token
    }
}
