package core.domain

import java.time.LocalDateTime

object DateExamples {
    val now = LocalDateTime.of(2021, 12, 6, 20, 20, 0)
}