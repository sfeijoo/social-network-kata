package core.domain

object UserExamples {
    val alice = "@alice"
    val alicePassword = "1234"
    val bob = "@bob"
    val bobPassword = "4321"
}