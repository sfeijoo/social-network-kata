package http.e2e

import com.eclipsesource.json.JsonObject
import console.SessionExamples
import console.SessionExamples.aliceSessionToken
import core.domain.UserExamples
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class PostE2ETest: E2ETests() {
    @Test
    fun `post message`() {
        givenAliceLogged()
        val postBody = JsonObject().add("message", message).toString()

        post("/posts", postBody, aliceSessionToken) Then {
            statusCode(200)
        }

        assertMessagePosted(message)
    }

    @Test
    fun `fail if anonymous user posts`() {
        Given {
            val postBody = JsonObject().add("message", message).toString()
            body(postBody)
        } When {
            post("$baseUrl/posts")
        } Then {
            statusCode(401)
        }
    }

    private fun givenAliceLogged() {
        useCaseProvider.signup().execute(UserExamples.alice, UserExamples.alicePassword)
        useCaseProvider.login().execute(UserExamples.alice, UserExamples.alicePassword)
    }

    private fun assertMessagePosted(message: String) {
        val response = useCaseProvider.read().execute(UserExamples.alice, SessionExamples.aliceSessionToken)
        Assertions.assertThat(response[0].message).isEqualTo(message)
    }

    private val message = "some message"
}
