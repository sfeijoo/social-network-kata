package http.e2e

import com.eclipsesource.json.JsonObject
import console.SessionExamples.aliceSessionToken
import core.domain.FollowingRepository
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.domain.UserExamples.bob
import core.domain.UserExamples.bobPassword
import core.domain.UserRepository
import core.domain.user.UserId
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class FollowE2ETests: E2ETests() {
    @Test
    fun `follow a user`() {
        val aliceId = givenAliceLogged()
        givenBobExists()
        val body = JsonObject().add("username", bob).toString()

        post("/users/$aliceId/followings/", body, aliceSessionToken) Then {
            statusCode(200)
        }

        assertAliceFollowsBob(aliceId)
    }

    @Test
    fun `unfollow a user`() {
        val aliceId = useCaseProvider.signup().execute(alice, alicePassword)
        useCaseProvider.login().execute(alice, alicePassword)
        useCaseProvider.signup().execute(bob, bobPassword)
        val followingId = useCaseProvider.follow().execute(bob, aliceSessionToken).toInt()

        Given {
            header("Authorization", "Bearer $aliceSessionToken")
        } When {
            delete("$baseUrl/users/${aliceId.toInt()}/followings/$followingId")
        } Then {
            statusCode(200)
        }

        val followingRepository = repositoryFactory.get(FollowingRepository::class)
        val aliceFollows = followingRepository.findByUser(aliceId)
        assertThat(aliceFollows).isEmpty()
    }

    private fun assertAliceFollowsBob(aliceId: UserId) {
        val followingRepository = repositoryFactory.get(FollowingRepository::class)
        val aliceFollows = followingRepository.findByUser(aliceId)
        val followee = repositoryFactory.get(UserRepository::class).getById(aliceFollows[0].followsId)

        assertThat(followee.userName).isEqualTo(bob)
    }

    private fun givenAliceLogged(): UserId {
        val id = useCaseProvider.signup().execute(alice, alicePassword)
        useCaseProvider.login().execute(alice, alicePassword)
        return id
    }

    private fun givenBobExists() {
        useCaseProvider.signup().execute(bob, bobPassword)
    }
}
