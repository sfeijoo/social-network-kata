package http.e2e

import console.SessionExamples.aliceSessionToken
import console.SessionExamples.bobSessionToken
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.domain.UserExamples.bob
import core.domain.UserExamples.bobPassword
import core.domain.user.UserId
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.Test

class WallE2ETests: E2ETests() {

    @Test
    fun `show user's wall`() {
        givenAlicePosted()

        val bobId = givenBobFollowsAlice()

        Given {
            header("Authorization", "Bearer $bobSessionToken")
        } When {
            get("$baseUrl/users/$bobId/wall")
        } Then {
            statusCode(200)
            body("posts[0].username", equalTo(alice))
            body("posts[0].message", equalTo(message))
        }
    }

    private fun givenBobFollowsAlice(): UserId {
        val bobId = useCaseProvider.signup().execute(bob, bobPassword)
        useCaseProvider.login().execute(bob, bobPassword)
        useCaseProvider.follow().execute(alice, bobSessionToken)
        return bobId
    }

    private fun givenAlicePosted() {
        useCaseProvider.signup().execute(alice, alicePassword)
        useCaseProvider.login().execute(alice, alicePassword)
        useCaseProvider.postMessage().execute(message, aliceSessionToken)
    }

    private val message = "a message"
}