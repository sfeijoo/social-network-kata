package http.e2e

import com.eclipsesource.json.JsonObject
import console.SessionExamples.aliceSessionToken
import core.domain.UserExamples
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import io.restassured.module.kotlin.extensions.Then
import io.restassured.response.ValidatableResponse
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.Test

class LoginE2ETests: E2ETests() {
    @Test
    fun login() {
        givenAliceExists()

        post("/login", ALICE_CREDENTIALS) Then {
            statusCode(200)
            body("isSuccessful", equalTo(true))
            body("sessionToken", equalTo(aliceSessionToken))
        }
    }

    @Test
    fun `login fail with invalid credentials`() {
        post("/login", INVALID_CREDENTIALS) Then {
            statusCode(200)
            body("isSuccessful", equalTo(false))
            body("sessionToken", equalTo(null))
        }
    }

    private fun givenAliceExists() {
        useCaseProvider.signup().execute(alice, alicePassword)
    }

    private val ALICE_CREDENTIALS = JsonObject().add("username", alice).add("password", alicePassword).toString()
    private val INVALID_CREDENTIALS = JsonObject().add("username", alice).add("password", "invalid").toString()
}
