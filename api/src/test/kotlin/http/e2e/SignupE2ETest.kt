package http.e2e

import com.eclipsesource.json.JsonObject
import core.domain.UserExamples
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import io.restassured.module.kotlin.extensions.Then
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.Test

class SignupE2ETest: E2ETests() {
    @Test
    fun `create user`() {
        post("/users", ALICE_REGISTRATION_INFO) Then {
            statusCode(200)
        }
        assertAliceExists()
    }

    @Test
    fun `fail if user already exists`() {
        useCaseProvider.signup().execute(alice, alicePassword)

        post("/users", ALICE_REGISTRATION_INFO) Then {
            statusCode(400)
            body("type", equalTo("UserAlreadyExistsError"))
            body("message", equalTo("User $alice already exists"))
        }
    }

    private fun assertAliceExists() {
        val response = useCaseProvider.login().execute(alice, alicePassword)
        assertThat(response.isSuccessful).isTrue
    }

    private val ALICE_REGISTRATION_INFO = JsonObject().add("username", alice).add("password", alicePassword).toString()
}
