package http.e2e

import console.SessionExamples.aliceSessionToken
import core.domain.DateExamples.now
import core.domain.UserExamples.alice
import core.domain.UserExamples.alicePassword
import core.domain.post.PostId
import core.infrastructure.CoreConfiguration
import core.infrastructure.UseCaseProvider
import core.useCases.FakeSessionTokenGenerator
import core.useCases.StoppedClock
import http.HttpApplication
import http.HttpApplicationConfiguration
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ReadE2ETests: E2ETests() {
        @Test
        fun `read posts`() {
            val postId = givenAlicePosted().toInt()
            Given {
                header("Authorization", "Bearer $aliceSessionToken")
            } When {
                get("$baseUrl/posts/search?username=$alice")
            } Then {
                statusCode(200)
                body("posts[0].id", equalTo(postId))
                body("posts[0].message", equalTo(message))
                body("posts[0].date", equalTo(clock.now().toString()))
            }
        }

        @Test
        fun `fail if anonymous user reads posts`() {
            When {
                get("$baseUrl/posts/search?username=$alice")
            } Then {
                statusCode(401)
            }
        }

        private fun givenAlicePosted(): PostId {
            useCaseProvider.signup().execute(alice, alicePassword)
            useCaseProvider.login().execute(alice, alicePassword)
            return useCaseProvider.postMessage().execute(message, aliceSessionToken)
        }

        private val message = "some message"
}