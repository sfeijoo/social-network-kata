package http.e2e

import console.SessionExamples
import core.domain.DateExamples
import core.infrastructure.CoreConfiguration
import core.infrastructure.inMemory.InMemoryRepositoryFactory
import core.infrastructure.UseCaseProvider
import core.useCases.FakeSessionTokenGenerator
import core.useCases.StoppedClock
import http.HttpApplication
import http.HttpApplicationConfiguration
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.When
import io.restassured.response.Response
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

abstract class E2ETests {

    @BeforeEach
    fun beforeEach() {
        app.start()
    }

    @AfterEach
    fun afterEach() {
        app.stop()
    }

    fun post(endpoint: String, body: String, sessionToken: String? = null): Response {
        return Given {
            if (sessionToken != null) {
                header("Authorization", "Bearer $sessionToken")
            }
            body(body)
        } When {
            post("$baseUrl$endpoint")
        }
    }

    protected val baseUrl = "http://localhost:6000"
    protected val repositoryFactory = InMemoryRepositoryFactory()
    protected val clock = StoppedClock.at(DateExamples.now)
    protected val useCaseProvider = UseCaseProvider(CoreConfiguration(clock, FakeSessionTokenGenerator(SessionExamples.aliceSessionToken), repositoryFactory))
    private val config = HttpApplicationConfiguration(6000, useCaseProvider)
    private val app = HttpApplication(config)
}
