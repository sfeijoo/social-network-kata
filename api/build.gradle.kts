import com.nbottarini.asimov.environment.Env
import nu.studer.gradle.jooq.JooqGenerate
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.10"
    id("nu.studer.jooq") version "6.0.1"
    id("org.flywaydb.flyway") version "7.11.2"
}

buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("com.nbottarini:asimov-environment:2.0.0")
    }
}

Env.addSearchPath(rootProject.projectDir.absolutePath)

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.5.31")
    implementation("io.javalin:javalin:4.1.1")
    implementation("com.nbottarini:asimov-environment:2.0.0")
    implementation("org.slf4j:slf4j-simple:1.7.30")
    implementation("com.eclipsesource.minimal-json:minimal-json:0.9.5")
    implementation("com.google.code.gson:gson:2.8.8")
    implementation("org.postgresql:postgresql:42.2.23")
    implementation("org.jooq:jooq:3.15.2")
    jooqGenerator("org.postgresql:postgresql:42.2.23")
    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.2")
    testImplementation("net.bytebuddy:byte-buddy:1.11.22") // Added for mockk compatibility with JDK16
    testImplementation("io.mockk:mockk:1.12.0")
    testImplementation("io.rest-assured:rest-assured:4.4.0")
    testImplementation("io.rest-assured:kotlin-extensions:4.4.0")
}

tasks.withType<KotlinCompile> { kotlinOptions.jvmTarget = "16" }

tasks.withType<Test> {
    useJUnitPlatform()
}

flyway {
    url = Env["DB_URL"]
    user = Env["DB_USER"]
    password = Env["DB_PASSWORD"]
    schemas = arrayOf("public")
    locations = arrayOf("filesystem:${project.projectDir}/src/main/resources/db")
}

jooq {
    version.set("3.15.2")

    configurations {
        create("main") {
            generateSchemaSourceOnCompilation.set(true)
            jooqConfiguration.apply {
                logging = org.jooq.meta.jaxb.Logging.WARN
                jdbc.apply {
                    driver = "org.postgresql.Driver"
                    url = Env["DB_URL"]
                    user = Env["DB_USER"]
                    password = Env["DB_PASSWORD"]
                }
                generator.apply {
                    name = "org.jooq.codegen.DefaultGenerator"
                    strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
                    database.apply {
                        name = "org.jooq.meta.postgres.PostgresDatabase"
                        inputSchema = "public"
                        includes = ".*"
                        excludes = ""
                        isIncludeSystemSequences = true
                    }
                    generate.apply {
                        isDeprecated = false
                        isRecords = true
                        isImmutablePojos = true
                        isFluentSetters = true
                    }
                    target.apply {
                        packageName = "core.infrastructure.db.generated"
                        directory = "src/main/jooq"
                    }
                }
            }
        }
    }
}

tasks.named<JooqGenerate>("generateJooq") {
    dependsOn("flywayMigrate")
    inputs.files(fileTree("src/main/resources/db"))
        .withPropertyName("migrations")
        .withPathSensitivity(PathSensitivity.RELATIVE)
    allInputsDeclared.set(true) // make jOOQ task participate in incremental builds and build caching
    outputs.cacheIf { true }
}